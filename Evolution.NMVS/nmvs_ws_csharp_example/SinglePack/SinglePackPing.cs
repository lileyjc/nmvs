﻿using System;
using Evolution.NMVS.SinglePackServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;

namespace Evolution.NMVS.SinglePack
{
    class SinglePackPing
    {
        /// <summary>
        /// Ping request to test the connection to the SinglePack-Services.
        /// Displays the XML request and the response.
        /// </summary>
        public static void PingSinglePack()
        {
            //add using directive Evolution.NMVS.SinglePackServices;
            //binding the service address you want to contact
            string EndPoint = "";

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate          
            SinglePackServicesClient service = new SinglePackServicesClient(binding, new EndpointAddress(EndPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2("", "", X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates PingRequest and set the ping Input
            SinglePackPingRequest ping = new SinglePackPingRequest();
            ping.Input = "Hello World!";

            //Creates PingResponse, result of the request. Displays the response
            SinglePackPingResponse pingResponse = service.PingSinglePack(ping);

            //Displays the response. If the request is successful, the output is equal to the input
            Console.WriteLine(pingResponse.Output);
        }
    }
}
