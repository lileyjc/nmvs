﻿using System;
using Evolution.NMVS.SinglePackServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;

namespace Evolution.NMVS.SinglePack
{

    class G120
    {
        public static object Header { get; set; }
        /// <summary>
        /// Creates a singlepack dispense request
        /// </summary>
        public static void G120Dispense()
        {
            //add using directive Evolution.NMVS.SinglePackServices;
            //binding the service address you want to contact
            string EndPoint = "";

            //Creates a G120Request object and a G120DispenseRequest object
            G120DispenseRequest request = new G120DispenseRequest();
            G120Request g120Request = new G120Request();

            //Executes the SetHeaderRequest method from the header class to fill the header
            object Header = Evolution.NMVS.Header.SetHeaderRequest("Evolution.NMVS.SinglePackServices", false);
            //Executes the SetSinglePackDispenseRequestBody method to fill the body
            object Body = SetSinglePackDispenseRequestBody();

            //Appends the header and body to the request object
            g120Request.Header = (RequestHeaderData_Type)Header;
            g120Request.Body = (RequestData_Type)Body;
            request.G120Request = g120Request;

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            SinglePackServicesClient service = new SinglePackServicesClient(binding, new EndpointAddress(EndPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2("","", X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates G120Response, result of the request.
            G120Response response = service.G120Dispense(g120Request);

            //Displays the response header and body.
            Console.WriteLine(response.Header);
            Console.WriteLine(response.Body);
        }

        /// <summary>
        /// Method to generate the body for a Singlepack dispense request
        /// </summary>
        /// <returns></returns>
        private static object SetSinglePackDispenseRequestBody()
        {
            //Set the serial number of a single pack
            SinglePackServices.RequestPack_Type pack = new SinglePackServices.RequestPack_Type();
            pack.sn = "ABCDEFG";

            //Set the product code and the schema of a single pack
            SinglePackServices.ProductIdentifier_Type productCode = new SinglePackServices.ProductIdentifier_Type();
            productCode.Value = "1234567891234";
            productCode.scheme = SinglePackServices.CatalogProductSchema_Type.PPN; // Alternativ GTIN

            //Set batch id and expiration date
            SinglePackServices.BaseBatch_Type batch = new SinglePackServices.BaseBatch_Type();
            batch.Id = "1";
            batch.ExpDate = "170721";

            //Append the productcode and batch objects to the RequestProduct_Type object
            SinglePackServices.RequestProduct_Type product = new SinglePackServices.RequestProduct_Type();
            product.ProductCode = productCode;
            product.Batch = batch;

            //Append the pack and product object to the RequestData_Type object
            SinglePackServices.RequestData_Type body = new SinglePackServices.RequestData_Type();
            body.Pack = pack;
            body.Product = product;
            return body;
            
        }
    }
}
