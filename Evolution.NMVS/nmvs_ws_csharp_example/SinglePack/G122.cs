﻿using System;
using Evolution.NMVS.SinglePackServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;


namespace Evolution.NMVS.SinglePack
{
    public class G122
    {
        public static object Header { get; set; }
        /// <summary>
        /// Creates a manual singlepack Dispense request
        /// </summary>
        public static void G122DispenseManualEntry()
        {
            //add using directive Evolution.NMVS.SinglePackServices;
            //binding the service address you want to contact
            string EndPoint = "";

            //Creates a G122Request object and a G122DispenseManualEntryRequest object
            G122DispenseManualEntryRequest request = new G122DispenseManualEntryRequest();
            G122Request g122Request = new G122Request();

            //Executes the SetHeaderRequest method from the header class to fill the header
            object Header = Evolution.NMVS.Header.SetHeaderRequest("Evolution.NMVS.SinglePackServices", false);
            //Executes the SetSinglePackManualDispenseRequestBody method to fill the body
            object Body = SetSinglePackManualDispenseRequestBody();

            //Appends the header and body to the request object
            g122Request.Header = (RequestHeaderData_Type)Header;
            g122Request.Body = (ManualRequestData_Type)Body;
            request.G122Request = g122Request;

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            SinglePackServicesClient service = new SinglePackServicesClient(binding, new EndpointAddress(EndPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2("", "", X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates G122Response, result of the request.
            G122Response response = service.G122DispenseManualEntry(g122Request);

            //Displays the response header and body.
            Console.WriteLine(response.Header);
            Console.WriteLine(response.Body);
        }

        /// <summary>
        /// Method to generate the body for a manual Singlepack dispense request
        /// </summary>
        /// <returns></returns>
        private static object SetSinglePackManualDispenseRequestBody()
        {
            //Set the serial number of a single pack
            SinglePackServices.RequestPack_Type pack = new SinglePackServices.RequestPack_Type();
            pack.sn = "ABCDEFG";

            //Set the product code and the schema of a single pack
            SinglePackServices.ProductIdentifier_Type productCode = new SinglePackServices.ProductIdentifier_Type();
            productCode.Value = "1234567891234";
            productCode.scheme = SinglePackServices.CatalogProductSchema_Type.GTIN; // Alternativ PPN
            
            //Append the productcode and batch objects to the RequestProduct_Type object
            SinglePackServices.ManualRequestData_TypeProduct product = new SinglePackServices.ManualRequestData_TypeProduct();
            product.ProductCode = productCode;

            //Append the pack and product object to the ManualRequestData_Type object
            ManualRequestData_Type body = new ManualRequestData_Type();
            body.Pack = pack;
            body.Product = product;
            return body;
        }
    }

}
