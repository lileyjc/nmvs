﻿using System;
using Evolution.NMVS.SinglePackServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;

namespace Evolution.NMVS.SinglePack
{

    class G112
    {
        public static object Header { get; set; }
        /// <summary>
        /// Creates a singlepack dispense request
        /// </summary>
        public static void G112ManualVerify()
        {
            //add using directive Evolution.NMVS.SinglePackServices;
            //binding the service address you want to contact
            string EndPoint = "";

            //Creates a request object and a manual verify request object
            G112ManualVerifyRequest request = new G112ManualVerifyRequest();
            G112Request g112Request = new G112Request();

            //Executes the SetHeaderRequest method from the header class to fill the header
            object Header = Evolution.NMVS.Header.SetHeaderRequest("Evolution.NMVS.SinglePackServices", false);
            //Executes the SetSinglePackDispenseRequestBody method to fill the body
            object Body = SetManualRequestDataTypeBody();

            //Appends the header and body to the request object
            g112Request.Header = (RequestHeaderData_Type)Header;
            g112Request.Body = (ManualRequestData_Type)Body;
            request.G112Request = g112Request;

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            SinglePackServicesClient service = new SinglePackServicesClient(binding, new EndpointAddress(EndPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2("","", X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates G112Response, result of the request.
            G112Response response = service.G112ManualVerify(g112Request);

            //Displays the response header and body.
            Console.WriteLine(response.Header);
            Console.WriteLine(response.Body);
        }

        /// <summary>
        /// Method to generate the body for a Singlepack dispense request
        /// </summary>
        /// <returns></returns>
        private static object SetManualRequestDataTypeBody()
        {
            //Set the serial number of a single pack
            SinglePackServices.RequestPack_Type pack = new SinglePackServices.RequestPack_Type();
            pack.sn = "ABCDEFG";

            //Set the product code and the schema of a single pack
            SinglePackServices.ProductIdentifier_Type productCode = new SinglePackServices.ProductIdentifier_Type();
            productCode.Value = "1234567891234";
            productCode.scheme = SinglePackServices.CatalogProductSchema_Type.PPN; // Alternativ GTIN

            //Append the productcode and batch objects to the RequestProduct_Type object
            SinglePackServices.ManualRequestData_TypeProduct product = new SinglePackServices.ManualRequestData_TypeProduct();
            product.ProductCode = productCode;

            //Append the pack and product object to the RequestData_Type object
            SinglePackServices.ManualRequestData_Type body = new SinglePackServices.ManualRequestData_Type();
            body.Pack = pack;
            body.Product = product;
            return body;
            
        }
    }
}
