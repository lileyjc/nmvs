﻿using System;
using Evolution.NMVS.SinglePackServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;
using System.Net;

namespace Evolution.NMVS.SinglePack
{
    public class G150
    {

        private string securityCert;
        private string securityPassword;

        public static object Header { get; set; }
        /// <summary>
        /// Creates a singlepack sample request
        /// </summary>
        /// 

        public G150()
        {
            securityCert = "";
            securityPassword = "";
        }

        public G150(string cert, string password)
        {
            securityCert = cert;
            securityPassword = password;
        }



        public G150Response G150Sample(Object body, string endPoint, string clientId, string userId, string userPwd)
        {
            //add using directive Evolution.NMVS.SinglePackServices;
            //binding the service address you want to contact
            //string EndPoint = "";

            //Creates a G150Request object and a G150SampleRequest object
            G150SampleRequest request = new G150SampleRequest();
            G150Request g150Request = new G150Request();

            //Executes the SetHeaderRequest method from the header class to fill the header
            object Header = Evolution.NMVS.Header.SetHeaderRequest("Evolution.NMVS.SinglePackServices", false,clientId,userId,userPwd);
            //Executes the SetSinglePackSampleRequestBody method to fill the body
            //object Body = SetSinglePackSampleRequestBody();

            //Appends the header and body to the request object
            g150Request.Header = (RequestHeaderData_Type)Header;
            g150Request.Body = (RequestData_Type)body;
            request.G150Request = g150Request;

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //added this line because for some reason the back end must have changed
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;


            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            SinglePackServicesClient service = new SinglePackServicesClient(binding, new EndpointAddress(endPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2(securityCert, securityPassword, X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates G150Response, result of the request.
            G150Response response = service.G150Sample(g150Request);

            //Displays the response header and body.
            //Console.WriteLine(response.Header);
            //Console.WriteLine(response.Body);

            return response;

        }

        /// <summary>
        /// Method to generate the body for a Singlepack sample request
        /// </summary>
        /// <returns></returns>
        private static object SetSinglePackSampleRequestBody()
        {
            //Set the serial number of a single pack
            SinglePackServices.RequestPack_Type pack = new SinglePackServices.RequestPack_Type();
            pack.sn = "ABCDEFG";

            //Set the product code and the schema of a single pack
            SinglePackServices.ProductIdentifier_Type productCode = new SinglePackServices.ProductIdentifier_Type();
            productCode.Value = "1234567891234";
            productCode.scheme = SinglePackServices.CatalogProductSchema_Type.GTIN; // Alternativ PPN

            //Set batch id and expiration date
            SinglePackServices.BaseBatch_Type batch = new SinglePackServices.BaseBatch_Type();
            batch.Id = "1";
            batch.ExpDate = "170721";

            //Append the productcode and batch objects to the RequestProduct_Type object
            SinglePackServices.RequestProduct_Type product = new SinglePackServices.RequestProduct_Type();
            product.ProductCode = productCode;
            product.Batch = batch;

            //Append the pack and product object to the RequestData_Type object
            SinglePackServices.RequestData_Type body = new SinglePackServices.RequestData_Type();
            body.Pack = pack;
            body.Product = product;
            return body;
        }
    }

}
