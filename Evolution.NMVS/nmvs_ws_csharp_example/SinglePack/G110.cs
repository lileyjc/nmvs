﻿using System;
using Evolution.NMVS.SinglePackServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;
using System.Net;

namespace Evolution.NMVS.SinglePack
{
    public class G110
    {

        private string securityCert;
        private string securityPassword;

        public static object Header { get; set; }
        /// <summary>
        /// Creates a singlepack verify request
        /// </summary>
        /// 

        public G110()
        {
            securityCert = "";
            securityPassword = "";
        }

        public G110(string cert,string password)
        {
            securityCert = cert;
            securityPassword = password;
        }

        public G110Response G110Verify(Object body, string endPoint, string clientId, string userId, string userPwd)
        {

            //add using directive Evolution.NMVS.SinglePackServices;
            //binding the service address you want to contact

            //string EndPoint = "";

            //Creates a G110Request object and a G110VerifyRequest object
            G110VerifyRequest request = new G110VerifyRequest();
            G110Request g110Request = new G110Request();

            //Executes the SetHeaderRequest method from the header class to fill the header
            object Header = Evolution.NMVS.Header.SetHeaderRequest("Evolution.NMVS.SinglePackServices", false,clientId,userId,userPwd);
            //Executes the SetSinglePackVerifyRequestBody method to fill the body
            //object Body = SetSinglePackVerifyRequestBody();

            //Appends the header and body to the request object
            g110Request.Header = (RequestHeaderData_Type)Header;
            g110Request.Body = (RequestData_Type)body;

            request.G110Request = g110Request;

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //added this line because for some reason the back end must have changed
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;


            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            SinglePackServicesClient service = new SinglePackServicesClient(binding, new EndpointAddress(endPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2(securityCert,securityPassword, X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates G110Response, result of the request.
            G110Response response = service.G110Verify(g110Request);

            //Displays the response header and body.
            //Console.WriteLine(response.Header);
            //Console.WriteLine(response.Body);

            return response;

        }

        /// <summary>
        /// Method to generate the body for a Singlepack verify request
        /// </summary>
        /// <returns></returns>
        private static object SetSinglePackVerifyRequestBody()
        {
            //Set the serial number of a single pack
            SinglePackServices.RequestPack_Type pack = new SinglePackServices.RequestPack_Type();
            pack.sn = "ABCDEFG";

            //Set the product code and the schema of a single pack
            SinglePackServices.ProductIdentifier_Type productCode = new SinglePackServices.ProductIdentifier_Type();
            productCode.Value = "1234567891234";
            productCode.scheme = SinglePackServices.CatalogProductSchema_Type.GTIN; // Alternativ PPN

            //Set batch id and expiration date
            SinglePackServices.BaseBatch_Type batch = new SinglePackServices.BaseBatch_Type();
            batch.Id = "1";
            batch.ExpDate = "170721";

            //Append the productcode and batch objects to the RequestProduct_Type object
            SinglePackServices.RequestProduct_Type product = new SinglePackServices.RequestProduct_Type();
            product.ProductCode = productCode;
            product.Batch = batch;
            
            //Append the pack and product object to the RequestData_Type object
            SinglePackServices.RequestData_Type body = new SinglePackServices.RequestData_Type();
            body.Pack = pack;
            body.Product = product;
            return body;
        }        
    }

}
