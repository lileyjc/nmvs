﻿using System;
using Evolution.NMVS.SinglePackServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;
using System.Net;

namespace Evolution.NMVS.SinglePack
{
    class G171
    {

        private string securityCert;
        private string securityPassword;

        public static object Header { get; set; }

        /// <summary>
        /// Creates a singlepack request to undo an lock
        /// </summary>
        /// 

        public G171()
        {
            securityCert = "";
            securityPassword = "";
        }

        public G171(string cert, string password)
        {
            securityCert = cert;
            securityPassword = password;
        }

        public G171Response G171UndoLock(Object body, string endPoint, string clientId, string userId, string userPwd)
        {
            //add using directive Evolution.NMVS.SinglePackServices;
            //binding the service address you want to contact
            //string EndPoint = "";

            //Creates a G171Request object and a G171UndoLockRequest object
            G171UndoLockRequest request = new G171UndoLockRequest();
            G171Request g171Request = new G171Request();

            //Executes the SetHeaderRequest method from the header class to fill the header
            object Header = Evolution.NMVS.Header.SetHeaderRequest("Evolution.NMVS.SinglePackServices", false,clientId,userId,userPwd);
            //Executes the SetSinglePackUndoLockRequestBody method to fill the body
            //object Body = SetSinglePackUndoLockRequestBody();

            //Appends the header and body to the request object
            g171Request.Header = (RequestHeaderData_Type)Header;
            g171Request.Body = (RequestUndoSingleData_Type)body;
            request.G171Request = g171Request;

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //added this line because for some reason the back end must have changed
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;


            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            SinglePackServicesClient service = new SinglePackServicesClient(binding, new EndpointAddress(endPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2(securityCert, securityPassword, X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates G171Response, result of the request.
            G171Response response = service.G171UndoLock(g171Request);

            //Displays the response header and body.
            //Console.WriteLine(response.Header);
            //Console.WriteLine(response.Body);

            return response;

        }

        /// <summary>
        /// Method to generate the body for a Singlepack lock undo request
        /// </summary>
        /// <returns></returns>
        private static object SetSinglePackUndoLockRequestBody()
        {
            //Set the serial number of a single pack
            SinglePackServices.RequestPack_Type pack = new SinglePackServices.RequestPack_Type();
            pack.sn = "ABCDEFG";

            //Set the product code and the schema of a single pack
            SinglePackServices.ProductIdentifier_Type productCode = new SinglePackServices.ProductIdentifier_Type();
            productCode.Value = "1234567891234";
            productCode.scheme = SinglePackServices.CatalogProductSchema_Type.GTIN; // Alternativ PPN

            //Set batch id and expiration date
            SinglePackServices.BaseBatch_Type batch = new SinglePackServices.BaseBatch_Type();
            batch.Id = "1";
            batch.ExpDate = "170721";

            //Append the productcode and batch objects to the RequestProduct_Type object
            SinglePackServices.RequestProduct_Type product = new SinglePackServices.RequestProduct_Type();
            product.ProductCode = productCode;
            product.Batch = batch;

            //Append the pack and product object to the RequestUndoSingleData_type object, and set the guid as RefClientTrxId
            SinglePackServices.RequestUndoSingleData_Type bodyUndo = new SinglePackServices.RequestUndoSingleData_Type();
            bodyUndo.Pack = pack;
            bodyUndo.Product = product;
            //Optional: bodyUndo.RefClientTrxId = "06c8dea069c24446949e7806e028bcaf"; //Alternativ: Guid.NewGuid().ToString().Replace("-", "");
            return bodyUndo;
        }
    }
}
