﻿using System;
using Evolution.NMVS.SinglePackServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;

namespace Evolution.NMVS.SinglePack
{
    class G123
    {
        public static object Header { get; set; }

        /// <summary>
        /// Creates a singlepack request to undo an dispense
        /// </summary>
        public static void G123UndoDispenseManualEntry()
        {
            //add using directive Evolution.NMVS.SinglePackServices;
            //binding the service address you want to contact
            string EndPoint = "";

            //Creates a G123Request object and a G123UndoDispenseRequest object
            G123UndoDispenseManualEntryRequest request = new G123UndoDispenseManualEntryRequest();
            G123Request G123Request = new G123Request();

            //Executes the SetHeaderRequest method from the header class to fill the header
            object Header = Evolution.NMVS.Header.SetHeaderRequest("Evolution.NMVS.SinglePackServices", true);
            //Executes the SetSinglePackUndoDispenseRequestBody method to fill the body
            object Body = SetRequestUndoManualSingleDataTypeBody();

            //Appends the header and body to the request object
            G123Request.Header = (RequestHeaderData_Type)Header;
            G123Request.Body = (RequestUndoManualSingleData_Type)Body;
            request.G123Request = G123Request;

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            SinglePackServicesClient service = new SinglePackServicesClient(binding, new EndpointAddress(EndPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2("", "", X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates G123Response, result of the request.
            G123Response response = service.G123UndoDispenseManualEntry(G123Request);

            //Displays the response header and body.
            Console.WriteLine(response.Header);
            Console.WriteLine(response.Body);
        }

        /// <summary>
        /// Method to generate the body for a Singlepack dispense undo request
        /// </summary>
        /// <returns></returns>
        private static object SetRequestUndoManualSingleDataTypeBody()
        {
            //Set the serial number of a single pack
            SinglePackServices.RequestPack_Type pack = new SinglePackServices.RequestPack_Type();
            pack.sn = "ABCDEFG";

            //Set the product code and the schema of a single pack
            SinglePackServices.ProductIdentifier_Type productCode = new SinglePackServices.ProductIdentifier_Type();
            productCode.Value = "1234567891234";
            productCode.scheme = SinglePackServices.CatalogProductSchema_Type.GTIN; // Alternativ PPN

            //Append the productcode and batch objects to the RequestProduct_Type object
            SinglePackServices.ManualRequestData_TypeProduct product = new SinglePackServices.ManualRequestData_TypeProduct();
            product.ProductCode = productCode;

            //Append the pack and product object to the RequestUndoSingleData_type object, and set the guid as RefClientTrxId
            SinglePackServices.RequestUndoManualSingleData_Type bodyUndo = new SinglePackServices.RequestUndoManualSingleData_Type();
            bodyUndo.Pack = pack;
            bodyUndo.Product = product;
            bodyUndo.RefClientTrxId = "06c8dea069c24446949e7806e028bcaf"; //Alternativ: Guid.NewGuid().ToString().Replace("-", "");
            return bodyUndo;            
        }
    }
}
