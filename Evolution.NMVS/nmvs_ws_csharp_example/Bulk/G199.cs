﻿using System;
using Evolution.NMVS.BulkServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;
using System.Net;

namespace Evolution.NMVS.Bulk
{
    class G199
    {
        public static object Header { get; set; }

        /// <summary>
        /// Creates a Bulk Transaction-result request
        /// </summary>
        public static void G199RequestPickupIdsForBulkTransactionResult()
        {
            //add using directive Evolution.NMVS.BulkServices;
            //binding the service address you want to contact
            string EndPoint = "";

            //Creates a G199Request object and a G199RequestPickupIdsForBulkTransactionResultRequest object
            G199RequestPickupIdsForBulkTransactionResultRequest request = new G199RequestPickupIdsForBulkTransactionResultRequest();
            G199Request g199Request = new G199Request();

            //Executes the SetHeaderRequest method from the header class to fill the header
            object Header = Evolution.NMVS.Header.SetHeaderRequest("Evolution.NMVS.BulkServices", false);

            //Appends the header and body to the request object
            g199Request.Header = (RequestHeaderData_Type)Header;
            request.G199Request = g199Request;

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //added this line because for some reason the back end must have changed
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;


            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            BulkServicesClient service = new BulkServicesClient(binding, new EndpointAddress(EndPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2("", "", X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates G199Response, result of the request.
            G199Response response = service.G199RequestPickupIdsForBulkTransactionResult(g199Request);

            //Displays the response header and body.
            Console.WriteLine(response.Header);
            Console.WriteLine(response.Body);
        }
    }
}
