﻿using System;
using Evolution.NMVS.BulkServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;
using System.Net;

namespace Evolution.NMVS.Bulk
{
    class G188
    {

        private string securityCert;
        private string securityPassword;

        public static object Header { get; set; }

        /// <summary>
        /// Creates a Bulk Pack result request
        /// </summary>
        /// 


        public G188()
        {
            securityCert = "";
            securityPassword = "";
        }

        public G188(string cert, string password)
        {
            securityCert = cert;
            securityPassword = password;
        }

        public G188Response G188RequestBulkPackResult(Object body, string endPoint, string clientId, string userId, string userPwd)
        {
            //add using directive Evolution.NMVS.BulkServices;
            //binding the service address you want to contact
            //string EndPoint = "";

            //Creates a G188Request object and a G188RequestBulkPackResultRequest object
            G188RequestBulkPackResultRequest request = new G188RequestBulkPackResultRequest();
            G188Request g188Request = new G188Request();

            //Executes the SetHeaderRequest method from the header class to fill the header
            object Header = Evolution.NMVS.Header.SetHeaderRequest("Evolution.NMVS.BulkServices", false,clientId,userId,userPwd);
            //Executes the SetBulkPackResultRequestBody method to fill the body
            //object Body = SetBulkPackResultRequestBody();

            //Appends the header and body to the request object
            g188Request.Header = (RequestHeaderData_Type)Header;
            g188Request.Body = (I6_ResultTransactionData_Type)body;
            request.G188Request = g188Request;

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //added this line because for some reason the back end must have changed
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;


            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            BulkServicesClient service = new BulkServicesClient(binding, new EndpointAddress(endPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2(securityCert, securityPassword, X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates G188Response, result of the request.
            G188Response response = service.G188RequestBulkPackResult(g188Request);

            //Displays the response header and body.
            //Console.WriteLine(response.Header);
            //Console.WriteLine(response.Body);

            return response;

        }
        /// <summary>
        /// Method to generate the body for a Bulk pack result request
        /// </summary>
        /// <returns></returns>
        private static object SetBulkPackResultRequestBody()
        {
            //set the guid as RefClientTrxId, append to the I6_ResultTransactionData_Type object
            I6_ResultTransactionData_Type body = new I6_ResultTransactionData_Type();
            body.RefNMVSTrxId = "06c8dea069c24446949e7806e028bcaf"; //Alternativ: Guid.NewGuid().ToString().Replace("-", "");
            return body;
        }
    }
}
