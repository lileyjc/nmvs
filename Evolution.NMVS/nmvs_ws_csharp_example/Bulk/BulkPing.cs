﻿using System;
using Evolution.NMVS.BulkServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;

namespace Evolution.NMVS.Bulk
{
    class BulkPing
    {
        /// <summary>
        /// Ping request to test the connection to the BulkServices.
        /// Displays the XML request and the response.
        /// </summary>
        public static void PingBulk()
        {
            //add using directive Evolution.NMVS.BulkServices;
            //binding the service address you want to contact
            string EndPoint = "";

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //Create ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            BulkServicesClient service = new BulkServicesClient(binding, new EndpointAddress(EndPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2("", "", X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Create PingRequest and set the ping Input
            BulkPingRequest ping = new BulkPingRequest();
            ping.Input = "Hello World!";

            //Create PingResponse, result of the request. Displays the response
            BulkPingResponse pingResponse = service.PingBulk(ping);

            //Displays the response. If the request is successful, the output is equal to the input
            Console.WriteLine(pingResponse.Output);
        }
    }
}
