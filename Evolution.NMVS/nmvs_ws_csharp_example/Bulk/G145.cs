﻿using System;
using Evolution.NMVS.BulkServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;

namespace Evolution.NMVS.Bulk
{
    class G145
    {
        public static object Header { get; set; }

        /// <summary>
        /// Creates a Bulk export request
        /// </summary>
        public static void G145BulkExport()
        {
            //add using directive Evolution.NMVS.BulkServices;
            //binding the service address you want to contact
            string EndPoint = "";

            //Creates a G145Request object and a G145BulkExportRequest object
            G145BulkExportRequest request = new G145BulkExportRequest();
            G145Request g145Request = new G145Request();

            //Executes the SetHeaderRequest method from the header class to fill the header
            object Header = Evolution.NMVS.Header.SetHeaderRequest("Evolution.NMVS.BulkServices", false);
            //Executes the SetBulkExportRequestBody method to fill the body
            object Body = SetBulkExportRequestBody();

            //Appends the header and body to the request object
            g145Request.Header = (RequestHeaderData_Type)Header;
            g145Request.Body = (RequestBulkData_Type)Body;
            request.G145Request = g145Request;

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            BulkServicesClient service = new BulkServicesClient(binding, new EndpointAddress(EndPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2("", "", X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates G145Response, result of the request.
            G145Response response = service.G145BulkExport(g145Request);

            //Displays the response header and body.
            Console.WriteLine(response.Header);
            Console.WriteLine(response.Body);
        }
        /// <summary>
        /// Method to generate the body for an Bulk Export request
        /// </summary>
        /// <returns></returns>
        private static object SetBulkExportRequestBody()
        {
            BulkServices.RequestPack_Type pack = new BulkServices.RequestPack_Type();
            List<RequestPack_Type> mylist = new List<RequestPack_Type>();
            BulkServices.RequestPack_Type[] Packs;
            pack.sn = "ABCDEFG";//A simple way to create multiple serial numbers to simulate the functionality of BulkRequest
            mylist.Add(pack);
            pack = new BulkServices.RequestPack_Type();
            pack.sn = "HIJKLMN";
            mylist.Add(pack);
            //pack = new BulkServices.RequestPack_Type();
            //pack.sn = "OPQRSTU";
            //mylist.Add(pack);
            //...
            Packs = mylist.ToArray();

            //Set the product code and the schema of a single pack
            BulkServices.ProductIdentifier_Type productCode = new BulkServices.ProductIdentifier_Type();
            productCode.Value = "1234567891234";
            productCode.scheme = BulkServices.CatalogProductSchema_Type.GTIN; // Alternativ PPN

            //Set batch id and expiration date
            BulkServices.BaseBatch_Type batch = new BulkServices.BaseBatch_Type();
            batch.Id = "1";
            batch.ExpDate = "170721";

            //Append the productcode and batch objects to the RequestProduct_Type object
            BulkServices.RequestProduct_Type product = new BulkServices.RequestProduct_Type();
            product.ProductCode = productCode;
            product.Batch = batch;

            //Append the pack and product object to the RequestUndoBulkData_Type object
            BulkServices.RequestBulkData_Type body = new BulkServices.RequestBulkData_Type();
            body.Packs = Packs;
            body.Product = product;
            return body;
        }
    }
}
