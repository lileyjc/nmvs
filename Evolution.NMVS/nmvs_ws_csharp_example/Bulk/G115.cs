﻿using System;
using Evolution.NMVS.BulkServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;
using System.Net;

namespace Evolution.NMVS.Bulk
{
    class G115
    {


        private string securityCert;
        private string securityPassword;

        public static object Header { get; set; }

        /// <summary>
        /// Creates a Bulk verify request
        /// </summary>
        /// 

        public G115()
        {
            securityCert = "";
            securityPassword = "";
        }

        public G115(string cert, string password)
        {
            securityCert = cert;
            securityPassword = password;
        }



        public G115Response G115BulkVerify(Object body, string endPoint, string clientId, string userId, string userPwd)
        {
            //add using directive Evolution.NMVS.BulkServices;
            //binding the service address you want to contact
            //string EndPoint = "";

            //Creates a G115Request object and a G115BulkVerifyRequest object
            G115BulkVerifyRequest request = new G115BulkVerifyRequest();
            G115Request g115Request = new G115Request();

            //Executes the SetHeaderRequest method from the header class to fill the header
            object Header = Evolution.NMVS.Header.SetHeaderRequest("Evolution.NMVS.BulkServices", false,clientId,userId,userPwd);
            //Executes the SetBulkVerifyRequestBody method to fill the body
            //object Body = SetBulkVerifyRequestBody();

            //Appends the header and body to the request object
            g115Request.Header = (RequestHeaderData_Type)Header;
            g115Request.Body = (RequestBulkData_Type)body;
            request.G115Request = g115Request;

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //added this line because for some reason the back end must have changed
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;


            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            BulkServicesClient service = new BulkServicesClient(binding, new EndpointAddress(endPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2(securityCert, securityPassword, X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates G115Response, result of the request.
            G115Response response = service.G115BulkVerify(g115Request);

            //Displays the response header and body.
            //Console.WriteLine(response.Header);
            //Console.WriteLine(response.Body);

            return response;

        }

        /// <summary>
        /// Method to generate the body for an Bulk verify request
        /// </summary>
        /// <returns></returns>
        private static object SetBulkVerifyRequestBody()
        {
            BulkServices.RequestPack_Type pack = new BulkServices.RequestPack_Type();
            List<RequestPack_Type> mylist = new List<RequestPack_Type>();
            BulkServices.RequestPack_Type[] Packs;
            pack.sn = "ABCDEFG";//A simple way to create multiple serial numbers to simulate the functionality of BulkRequest
            mylist.Add(pack);
            pack = new BulkServices.RequestPack_Type();
            pack.sn = "HIJKLMN";
            mylist.Add(pack);
            //pack = new BulkServices.RequestPack_Type();
            //pack.sn = "OPQRSTU";
            //mylist.Add(pack);
            //...
            Packs = mylist.ToArray();

            //Set the product code and the schema of a single pack
            BulkServices.ProductIdentifier_Type productCode = new BulkServices.ProductIdentifier_Type();
            productCode.Value = "1234567891234";
            productCode.scheme = BulkServices.CatalogProductSchema_Type.GTIN; // Alternativ PPN

            //Set batch id and expiration date
            BulkServices.BaseBatch_Type batch = new BulkServices.BaseBatch_Type();
            batch.Id = "1";
            batch.ExpDate = "170721";

            //Append the productcode and batch objects to the RequestProduct_Type object
            BulkServices.RequestProduct_Type product = new BulkServices.RequestProduct_Type();
            product.ProductCode = productCode;
            product.Batch = batch;

            //Append the pack and product object to the RequestUndoBulkData_Type object
            BulkServices.RequestBulkData_Type body = new BulkServices.RequestBulkData_Type();
            body.Packs = Packs;
            body.Product = product;
            return body;
        }
    }
}
