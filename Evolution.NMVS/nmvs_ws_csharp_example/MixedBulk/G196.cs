﻿using System;
using Evolution.NMVS.MixedBulkServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;

namespace Evolution.NMVS.MixedBulk
{
    class G196
    {
        public static object Header { get; set; }

        /// <summary>
        /// Creates a MixedBulk transaction result request
        /// </summary>
        public static void G196RequestBulkTransactionResult()
        {
            //add using directive Evolution.NMVS.MixedBulkServices;
            //binding the service address you want to contact
            string EndPoint = "";


            //Creates a G196Request object and a G196RequestBulkTransactionResultRequest object
            G196RequestBulkTransactionResultRequest request = new G196RequestBulkTransactionResultRequest();
            G196Request g196Request = new G196Request();

            //Executes the SetHeaderRequest method from the header class to fill the header
            object Header = Evolution.NMVS.Header.SetHeaderRequest("Evolution.NMVS.MixedBulkServices", false);
            //Executes the SetMixedBulkRequestBody method to fill the body
            object Body = SetMixedBulkRequestBody();

            //Appends the header and body to the request object
            g196Request.Header = (RequestHeaderData_Type)Header;
            g196Request.Body = (I6_ResultTransactionData_Type)Body;
            request.G196Request = g196Request;

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            MixedBulkServicesClient service = new MixedBulkServicesClient(binding, new EndpointAddress(EndPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2("", "", X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates G196Response, result of the request.
            G196Response response = service.G196RequestBulkTransactionResult(g196Request);
            
            //Displays the response header and body.
            Console.WriteLine(response.Header);
            Console.WriteLine(response.Body);
        }

        /// <summary>
        /// Method to generate the body for a MixedBulk transaction result request
        /// </summary>
        /// <returns></returns>
        private static object SetMixedBulkRequestBody()
        {
            //set the guid as RefClientTrxId, append to the I6_ResultTransactionData_Type object
            I6_ResultTransactionData_Type body = new I6_ResultTransactionData_Type();
            body.RefNMVSTrxId = "06c8dea069c24446949e7806e028bcaf"; //Alternativ: Guid.NewGuid().ToString().Replace("-", "");
            return body;
        }
    }
}
