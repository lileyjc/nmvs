﻿using System;
using System.IO;
using System.Xml.Serialization;
using Evolution.NMVS.MixedBulkServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;

namespace Evolution.NMVS.MixedBulk
{
    class G195
    {
        public static object Header { get; set; }

        /// <summary>
        /// Creates a MixedBulk transaction submit
        /// </summary>
        public static void G195SubmitBulkTransaction()
        {
            //add using directive Evolution.NMVS.MixedBulkServices;
            //binding the service address you want to contact
            string EndPoint = "";

            //Creates a G195Request object and a G195SubmitBulkTransactionRequest object
            G195SubmitBulkTransactionRequest request = new G195SubmitBulkTransactionRequest();
            G195Request g195Request = new G195Request();

            //Executes the SetHeaderRequest method from the header class to fill the header
            object Header = Evolution.NMVS.Header.SetHeaderRequest("Evolution.NMVS.MixedBulkServices", false);
            //Executes the SetMixedBulkSubmitBody method to fill the body
            object Body = SetMixedBulkSubmitBody();

            //Appends the header and body to the request object
            g195Request.Header = (RequestHeaderData_Type)Header;
            g195Request.Body = (RequestMixedBulkData_Type)Body;
            request.G195Request = g195Request;

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            MixedBulkServicesClient service = new MixedBulkServicesClient(binding, new EndpointAddress(EndPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2("", "", X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            string objectStringRequest = SerializeToString<G195Request>(g195Request);
            Console.WriteLine(objectStringRequest);
            objectStringRequest = SerializeToString<G195SubmitBulkTransactionRequest>(request);
            Console.WriteLine(objectStringRequest);

            //Creates G195Response, result of the request.
            G195Response response = service.G195SubmitBulkTransaction(g195Request);

            //Displays the response header and body.
            Console.WriteLine(response.Header);
            Console.WriteLine(response.Body);
        }

        /// <summary>
        /// Attachs the desired number of items from the AddItem method to the list, which is to be sent
        /// </summary>
        /// <returns></returns>
        private static object SetMixedBulkSubmitBody()
        {
            List<MixedBulkServices.RequestMixedBulkData_TypeTrxItem> TryList = new List<RequestMixedBulkData_TypeTrxItem>();

            //A simple way to create multiple items to simulate the functionality of MixedBulk
            TryList.Add(AddItem());
            //TryList.Add(AddItem());
            //TryList.Add(AddItem());
            //....            

            //Appends the item list to the body
            MixedBulkServices.RequestMixedBulkData_Type body = new MixedBulkServices.RequestMixedBulkData_Type();
            body.TrxList = TryList.ToArray();
            return body;
        }

        /// <summary>
        /// Method to generate Items
        /// </summary>
        /// <returns></returns>
        private static MixedBulkServices.RequestMixedBulkData_TypeTrxItem AddItem()
        {
            //Set the serial number of a single pack
            MixedBulkServices.RequestPack_Type pack = new MixedBulkServices.RequestPack_Type();
            pack.sn = "ABCDEFG";

            //Set the product code and the schema of a single pack
            MixedBulkServices.ProductIdentifier_Type productCode = new MixedBulkServices.ProductIdentifier_Type();
            productCode.Value = "1234567891234";
            productCode.scheme = MixedBulkServices.CatalogProductSchema_Type.GTIN; // Alternativ PPN

            //Set batch id and expiration date
            MixedBulkServices.BaseBatch_Type batch = new MixedBulkServices.BaseBatch_Type();
            batch.Id = "1";
            batch.ExpDate = "170721";

            //Append the productcode and batch objects to the RequestProduct_Type object
            MixedBulkServices.RequestProduct_Type product = new MixedBulkServices.RequestProduct_Type();
            product.ProductCode = productCode;
            product.Batch = batch;

            //Append the pack and product object to the RequestUndoSingleData_type object
            MixedBulkServices.RequestData_Type requestData = new MixedBulkServices.RequestData_Type();
            requestData.Pack = pack;
            requestData.Product = product;

            //Set the a new guid as RefClientTrxId and ClientTrxId
            MixedBulkServices.RequestTransactionHeaderData_Type mixedBulkRequestTransaction = new MixedBulkServices.RequestTransactionHeaderData_Type();
            //TODO krie23 (26.08.2019 14:57:27): REfClientTrxId prüfen. ob sie noch gibt
            //mixedBulkRequestTransaction.RefClientTrxId = "06c8dea069c24446949e7806e028bcaf"; //Alternativ: Guid.NewGuid().ToString().Replace("-", "");
            mixedBulkRequestTransaction.ClientTrxId = "06c8dea069c24446949e7806e028bcaf";  //Alternativ: Guid.NewGuid().ToString().Replace("-", "");
            mixedBulkRequestTransaction.Language = "eng";

            MixedBulkServices.MixedSingle_Type item = new MixedBulkServices.MixedSingle_Type();
            item.Pack = pack;
            item.Product = product;
            item.reqType= MixedBulkServices.CatalogUseCaseMixed_Type.G110;
            item.Transaction = mixedBulkRequestTransaction;

            //Append Pack and Product object to the RequestMixedBulkItem_Type object an set the request Type
            MixedBulkServices.RequestMixedBulkData_TypeTrxItem trxItem = new MixedBulkServices.RequestMixedBulkData_TypeTrxItem();
            trxItem.Item = item;
            return trxItem;
        }

        private static string SerializeToString<T>(T objectToSerialize)
        {
            StringWriter sw = new StringWriter();
            XmlSerializer ser = new XmlSerializer(typeof(T));
            ser.Serialize(sw, objectToSerialize);

            return sw.ToString();
        }
    }
}
