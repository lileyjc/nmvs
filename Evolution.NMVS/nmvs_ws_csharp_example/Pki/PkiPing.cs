﻿using System;
using Evolution.NMVS.PKIServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;

namespace Evolution.NMVS.Pki
{
    class PkiPing
    {
        /// <summary>
        /// Ping request to test the connection to the Pki-Services.
        /// Displays the XML request and the response.
        /// </summary>
        public static void PingPki()
        {
            //add using directive Evolution.NMVS.PkiServices;
            //binding the service address you want to contact
            string EndPoint = "";

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //Create ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            PkiServicesClient service = new PkiServicesClient(binding, new EndpointAddress(EndPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2("", "", X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Create PingRequest and set the ping Input
            PkiPingRequest ping = new PkiPingRequest();
            ping.Input = "Hello World!";

            //Create PingResponse, result of the request. Displays the response
            PkiPingResponse pingResponse = service.PingPki(ping);

            //Displays the response. If the request is successful, the output is equal to the input
            Console.WriteLine(pingResponse.Output);
        }
    }
}
