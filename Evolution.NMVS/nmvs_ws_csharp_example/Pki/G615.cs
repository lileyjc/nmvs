﻿using System;
using Evolution.NMVS.PKIServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;

namespace Evolution.NMVS.Pki
{
    class G615
    {
        public static object Header { get; set; }

        /// <summary>
        /// Creates a DownloadClientCertificate request to download a new Certificate
        /// </summary>
        public static void G615DownloadClientCertificate()
        {
            //add using directive Evolution.NMVS.PkiServices;
            //binding the service address you want to contact
            string EndPoint = "";

            //Creates a G615Request object and a G615DownloadClientCertificateRequest object
            G615DownloadClientCertificateRequest request = new G615DownloadClientCertificateRequest();
            G615Request g615Request = new G615Request();

            //Executes the SetHeaderRequest method from the header class to fill the header
            object Header = Evolution.NMVS.Header.SetHeaderRequest("Evolution.NMVS.PKIServices", false);
            //Executes the SetPkiRequestBody method to fill the body
            object Body = SetPkiRequestBody();

            //Appends the header and body to the request object
            g615Request.Header = (RequestHeaderData_Type)Header;
            g615Request.Body = (DownloadCertRequestData_Type)Body;
            request.G615Request = g615Request;

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            PkiServicesClient service = new PkiServicesClient(binding, new EndpointAddress(EndPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2("", "", X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates G615Response, result of the request.
            G615Response response = service.G615DownloadClientCertificate(g615Request);
            
            //Displays the response header and body.
            Console.WriteLine(response.Header);
            Console.WriteLine(response.Body);
        }

        /// <summary>
        /// Method to generate the body for a Pki request
        /// </summary>
        /// <returns></returns>
        private static object SetPkiRequestBody()
        {
            //Creates the DownloadCertRequestData_Type object and appends the TAN
            PKIServices.DownloadCertRequestData_Type body = new PKIServices.DownloadCertRequestData_Type();
            body.Tan = "12345678";
            return body;
        }
    }
}
