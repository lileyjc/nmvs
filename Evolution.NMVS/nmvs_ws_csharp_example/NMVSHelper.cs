﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evolution.NMVS
{
    using Evolution.NMVS.BulkServices;
    // -----------------------------------------------------------------------
    // <copyright file="C:\Work\Projects\Evolution\Evolution.NMVS\NMVSHelper.vb" company="Visual Software Limited">
    // Author: James Liley
    // Copyright (c) 2010 Visual Software Limited. All rights reserved.
    // </copyright>
    // Imports Evolution.NMVS
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web;
    using System.Web.Script.Serialization;


    public partial class NMVSHelper
    {
        private string certPath;
        private string certPassword;
        private HttpContext context;
        private string _singlePackEndpoint;
        private string _bulkServicesEndPoint;
        private string clientId;
        private string userId;
        private string userPwd;
        private Stopwatch sw;
        private UIDHelper uidHelper;

        public partial class UIDInfo
        {
            public string PPN;
            public string GTIN;
            public string BatchNumber;
            public string Expiry;
            public DateTime ExpiryDate;
            public string SerialNumber;
            public string NHRN;
            public string Uid;
        }

        public NMVSHelper(HttpContext context, string groupSeparator) : this()
        {
            
            this.context = context;

            uidHelper = new UIDHelper();

            if (!string.IsNullOrEmpty(groupSeparator))
            {
                uidHelper.GroupSeperator = Convert.ToChar(groupSeparator);
            }
            
            CompanyID = Convert.ToInt32(context.Session["Company"]);
        }

        public NMVSHelper(int company, HttpContext context, string groupSeparator) : this()
        {
            
            this.context = context;

            uidHelper = new UIDHelper();

            if (!string.IsNullOrEmpty(groupSeparator))
            {
                uidHelper.GroupSeperator = Convert.ToChar(groupSeparator);
            }

            CompanyID = company;
        }

        private int _companyID;

        public int CompanyID
        {
            get
            {
                return _companyID;
            }

            set
            {
                _companyID = value;
                InitConfig();
            }
        }

        public NMVSHelper(string endPoint, string clientId, string userId, string userPwd, string certPath, string certPassword) : this()
        {
            _singlePackEndpoint = endPoint;
            this.clientId = clientId;
            this.userId = userId;
            this.userPwd = userPwd;
            this.certPath = certPath;
            this.certPassword = certPassword;
        }

        //public enum DataType
        //{
        //    Numeric,
        //    Alphanumeric
        //}

        //public partial class AII
        //{
        //    public string AI { get; set; }
        //    public string Description { get; set; }
        //    public int LengthOfAI { get; set; }
        //    public DataType DataDescription { get; set; }
        //    public int LengthOfData { get; set; }
        //    public bool FNC1 { get; set; }

        //    public AII(string AI, string Description, int LengthOfAI, DataType DataDescription, int LengthOfData, bool FNC1)
        //    {
        //        this.AI = AI;
        //        this.Description = Description;
        //        this.LengthOfAI = LengthOfAI;
        //        this.DataDescription = DataDescription;
        //        this.LengthOfData = LengthOfData;
        //        this.FNC1 = FNC1;
        //    }

        //    public override string ToString()
        //    {
        //        return string.Format("{0} [{1}]", AI, Description);
        //    }
        //}

        //private SortedDictionary<string, AII> aiiDict = new SortedDictionary<string, AII>();
        //private string[] aiis;
        //private int minLengthOfAI;
        //private int maxLengthOfAI;
        //private bool _hasCheckSum;

        //public bool HasCheckSum
        //{
        //    get
        //    {
        //        return _hasCheckSum;
        //    }

        //    set
        //    {
        //        _hasCheckSum = value;
        //    }
        //}

        //private char _groupSeperator;

        //public char GroupSeperator
        //{
        //    get
        //    {
        //        return _groupSeperator;
        //    }

        //    set
        //    {
        //        _groupSeperator = value;
        //    }
        //}

        //private string _ean128StartCode;

        //public string EAN128StartCode
        //{
        //    get
        //    {
        //        return _ean128StartCode;
        //    }

        //    set
        //    {
        //        _ean128StartCode = value;
        //    }
        //}

        public string SinglePackEndPoint
        {
            get
            {
                return _singlePackEndpoint;
            }

            set
            {
                _singlePackEndpoint = value;
            }
        }

        public string BulkServicesEndPoint
        {
            get
            {
                return _bulkServicesEndPoint;
            }

            set
            {
                _bulkServicesEndPoint = value;
            }
        }

        public Stopwatch Stopwatch
        {
            get
            {
                return sw;
            }
        }

        public string ResponseTime
        {
            get
            {
                return sw.ElapsedMilliseconds.ToString();
            }
        }

        public NMVSHelper()
        {
            sw = new Stopwatch();

            uidHelper = new UIDHelper();

            //aiiDict = new SortedDictionary<string, AII>();
            //HasCheckSum = false;
            //GroupSeperator = '^';
            //EAN128StartCode = "]C1";
            //minLengthOfAI = 1;
            //maxLengthOfAI = 4;

            //Add("00", "SerialShippingContainerCode", 2, DataType.Numeric, 18, false);
            //Add("01", "EAN-NumberOfTradingUnit", 2, DataType.Numeric, 14, false);
            //Add("02", "EAN-NumberOfTheWaresInTheShippingUnit", 2, DataType.Numeric, 14, false);
            //Add("10", "Batch_Number", 2, DataType.Alphanumeric, 20, true);
            //Add("11", "ProducerDate_JJMMDD", 2, DataType.Numeric, 6, false);
            //Add("12", "DueDate_JJMMDD", 2, DataType.Numeric, 6, false);
            //Add("13", "PackingDate_JJMMDD", 2, DataType.Numeric, 6, false);
            //Add("15", "MinimumDurabilityDate_JJMMDD", 2, DataType.Numeric, 6, false);
            //Add("17", "ExpiryDate_JJMMDD", 2, DataType.Numeric, 6, false);
            //Add("20", "ProductModel", 2, DataType.Numeric, 2, false);
            //Add("21", "SerialNumber", 2, DataType.Alphanumeric, 20, true);
            //Add("22", "HIBCCNumber", 2, DataType.Alphanumeric, 29, false);
            //Add("240", "PruductIdentificationOfProducer", 3, DataType.Alphanumeric, 30, true);
            //Add("241", "CustomerPartsNumber", 3, DataType.Alphanumeric, 30, true);
            //Add("250", "SerialNumberOfAIntegratedModule", 3, DataType.Alphanumeric, 30, true);
            //Add("251", "ReferenceToTheBasisUnit", 3, DataType.Alphanumeric, 30, true);
            //Add("252", "GlobalIdentifierSerialisedForTrade", 3, DataType.Numeric, 2, false);
            //Add("30", "AmountInParts", 2, DataType.Numeric, 8, true);
            //Add("310d", "NetWeight_Kilogram", 4, DataType.Numeric, 6, false);
            //Add("311d", "Length_Meter", 4, DataType.Numeric, 6, false);
            //Add("312d", "Width_Meter", 4, DataType.Numeric, 6, false);
            //Add("313d", "Heigth_Meter", 4, DataType.Numeric, 6, false);
            //Add("314d", "Surface_SquareMeter", 4, DataType.Numeric, 6, false);
            //Add("315d", "NetVolume_Liters", 4, DataType.Numeric, 6, false);
            //Add("316d", "NetVolume_CubicMeters", 4, DataType.Numeric, 6, false);
            //Add("320d", "NetWeight_Pounds", 4, DataType.Numeric, 6, false);
            //Add("321d", "Length_Inches", 4, DataType.Numeric, 6, false);
            //Add("322d", "Length_Feet", 4, DataType.Numeric, 6, false);
            //Add("323d", "Length_Yards", 4, DataType.Numeric, 6, false);
            //Add("324d", "Width_Inches", 4, DataType.Numeric, 6, false);
            //Add("325d", "Width_Feed", 4, DataType.Numeric, 6, false);
            //Add("326d", "Width_Yards", 4, DataType.Numeric, 6, false);
            //Add("327d", "Heigth_Inches", 4, DataType.Numeric, 6, false);
            //Add("328d", "Heigth_Feed", 4, DataType.Numeric, 6, false);
            //Add("329d", "Heigth_Yards", 4, DataType.Numeric, 6, false);
            //Add("330d", "GrossWeight_Kilogram", 4, DataType.Numeric, 6, false);
            //Add("331d", "Length_Meter", 4, DataType.Numeric, 6, false);
            //Add("332d", "Width_Meter", 4, DataType.Numeric, 6, false);
            //Add("333d", "Heigth_Meter", 4, DataType.Numeric, 6, false);
            //Add("334d", "Surface_SquareMeter", 4, DataType.Numeric, 6, false);
            //Add("335d", "GrossVolume_Liters", 4, DataType.Numeric, 6, false);
            //Add("336d", "GrossVolume_CubicMeters", 4, DataType.Numeric, 6, false);
            //Add("337d", "KilogramPerSquareMeter", 4, DataType.Numeric, 6, false);
            //Add("340d", "GrossWeight_Pounds", 4, DataType.Numeric, 6, false);
            //Add("341d", "Length_Inches", 4, DataType.Numeric, 6, false);
            //Add("342d", "Length_Feet", 4, DataType.Numeric, 6, false);
            //Add("343d", "Length_Yards", 4, DataType.Numeric, 6, false);
            //Add("344d", "Width_Inches", 4, DataType.Numeric, 6, false);
            //Add("345d", "Width_Feed", 4, DataType.Numeric, 6, false);
            //Add("346d", "Width_Yards", 4, DataType.Numeric, 6, false);
            //Add("347d", "Heigth_Inches", 4, DataType.Numeric, 6, false);
            //Add("348d", "Heigth_Feed", 4, DataType.Numeric, 6, false);
            //Add("349d", "Heigth_Yards", 4, DataType.Numeric, 6, false);
            //Add("350d", "Surface_SquareInches", 4, DataType.Numeric, 6, false);
            //Add("351d", "Surface_SquareFeet", 4, DataType.Numeric, 6, false);
            //Add("352d", "Surface_SquareYards", 4, DataType.Numeric, 6, false);
            //Add("353d", "Surface_SquareInches", 4, DataType.Numeric, 6, false);
            //Add("354d", "Surface_SquareFeed", 4, DataType.Numeric, 6, false);
            //Add("355d", "Surface_SquareYards", 4, DataType.Numeric, 6, false);
            //Add("356d", "NetWeight_TroyOunces", 4, DataType.Numeric, 6, false);
            //Add("357d", "NetVolume_Ounces", 4, DataType.Numeric, 6, false);
            //Add("360d", "NetVolume_Quarts", 4, DataType.Numeric, 6, false);
            //Add("361d", "NetVolume_Gallonen", 4, DataType.Numeric, 6, false);
            //Add("362d", "GrossVolume_Quarts", 4, DataType.Numeric, 6, false);
            //Add("363d", "GrossVolume_Gallonen", 4, DataType.Numeric, 6, false);
            //Add("364d", "NetVolume_CubicInches", 4, DataType.Numeric, 6, false);
            //Add("365d", "NetVolume_CubicFeet", 4, DataType.Numeric, 6, false);
            //Add("366d", "NetVolume_CubicYards", 4, DataType.Numeric, 6, false);
            //Add("367d", "GrossVolume_CubicInches", 4, DataType.Numeric, 6, false);
            //Add("368d", "GrossVolume_CubicFeet", 4, DataType.Numeric, 6, false);
            //Add("369d", "GrossVolume_CubicYards", 4, DataType.Numeric, 6, false);
            //Add("37", "QuantityInParts", 2, DataType.Numeric, 8, true);
            //Add("390d", "AmountDue_DefinedValutaBand", 4, DataType.Numeric, 15, true);
            //Add("391d", "AmountDue_WithISOValutaCode", 4, DataType.Numeric, 18, true);
            //Add("392d", "BePayingAmount_DefinedValutaBand", 4, DataType.Numeric, 15, true);
            //Add("393d", "BePayingAmount_WithISOValutaCode", 4, DataType.Numeric, 18, true);
            //Add("400", "JobNumberOfGoodsRecipient", 3, DataType.Alphanumeric, 30, true);
            //Add("401", "ShippingNumber", 3, DataType.Alphanumeric, 30, true);
            //Add("402", "DeliveryNumber", 3, DataType.Numeric, 17, false);
            //Add("403", "RoutingCode", 3, DataType.Alphanumeric, 30, true);
            //Add("410", "EAN_UCC_GlobalLocationNumber(GLN)_GoodsRecipient", 3, DataType.Numeric, 13, false);
            //Add("411", "EAN_UCC_GlobalLocationNumber(GLN)_InvoiceRecipient", 3, DataType.Numeric, 13, false);
            //Add("412", "EAN_UCC_GlobalLocationNumber(GLN)_Distributor", 3, DataType.Numeric, 13, false);
            //Add("413", "EAN_UCC_GlobalLocationNumber(GLN)_FinalRecipient", 3, DataType.Numeric, 13, false);
            //Add("414", "EAN_UCC_GlobalLocationNumber(GLN)_PhysicalLocation", 3, DataType.Numeric, 13, false);
            //Add("415", "EAN_UCC_GlobalLocationNumber(GLN)_ToBilligParticipant", 3, DataType.Numeric, 13, false);
            //Add("420", "ZipCodeOfRecipient_withoutCountryCode", 3, DataType.Alphanumeric, 20, true);
            //Add("421", "ZipCodeOfRecipient_withCountryCode", 3, DataType.Alphanumeric, 12, true);
            //Add("422", "BasisCountryOfTheWares_ISO3166Format", 3, DataType.Numeric, 3, false);
            //Add("7001", "Nato Stock Number", 4, DataType.Numeric, 13, false);
            //Add("710", "NationalHealthcareReimbursementNumberDE", 3, DataType.Alphanumeric, 20, true);
            //Add("711", "NationalHealthcareReimbursementNumberFR", 3, DataType.Alphanumeric, 20, true);
            //Add("712", "NationalHealthcareReimbursementNumberES", 3, DataType.Alphanumeric, 20, true);
            //Add("714", "NationalHealthcareReimbursementNumberPT", 3, DataType.Alphanumeric, 20, true);
            //Add("8001", "RolesProducts", 4, DataType.Numeric, 14, false);
            //Add("8002", "SerialNumberForMobilePhones", 4, DataType.Alphanumeric, 20, true);
            //Add("8003", "GlobalReturnableAssetIdentifier", 4, DataType.Alphanumeric, 34, true);
            //Add("8004", "GlobalIndividualAssetIdentifier", 4, DataType.Numeric, 30, true);
            //Add("8005", "SalesPricePerUnit", 4, DataType.Numeric, 6, false);
            //Add("8006", "IdentifikationOfAProductComponent", 4, DataType.Numeric, 18, false);
            //Add("8007", "IBAN", 4, DataType.Alphanumeric, 30, true);
            //Add("8008", "DataAndTimeOfManufacturing", 4, DataType.Numeric, 12, true);
            //Add("8018", "GlobalServiceRelationNumber", 4, DataType.Numeric, 18, false);
            //Add("8020", "NumberBillCoverNumber", 4, DataType.Alphanumeric, 25, false);
            //Add("8100", "CouponExtendedCode_NSC_offerCcode", 4, DataType.Numeric, 10, false);
            //Add("8101", "CouponExtendedCode_NSC_offerCcode_EndOfOfferCode", 4, DataType.Numeric, 14, false);
            //Add("8102", "CouponExtendedCode_NSC", 4, DataType.Numeric, 6, false);
            //Add("90", "InformationForBilateralCoordinatedApplications", 2, DataType.Alphanumeric, 30, true);
            //Add("9N", "ASC PPN", 2, DataType.Alphanumeric, 22, true);
            //Add("8P", "ASC GTIN", 2, DataType.Numeric, 14, true);
            //Add("1T", "ASC Batch", 2, DataType.Alphanumeric, 20, true);
            //Add("D", "ASC Expiry", 1, DataType.Numeric, 6, true);
            //Add("S", "ASC Serial", 1, DataType.Alphanumeric, 20, true);
            //aiis = aiiDict.Keys.ToArray();
            //minLengthOfAI = aiiDict.Values.Min(el => el.LengthOfAI);
            //maxLengthOfAI = aiiDict.Values.Max(el => el.LengthOfAI);
        }

        //public void Add(string AI, string Description, int LengthOfAI, DataType DataDescription, int LengthOfData, bool FNC1)
        //{
        //    aiiDict[AI] = new AII(AI, Description, LengthOfAI, DataDescription, LengthOfData, FNC1);
        //}

        //public Dictionary<string, string> Parse(string data, bool throwException = false)
        //{
        //    if (data.StartsWith(EAN128StartCode))
        //        data = data.Substring(EAN128StartCode.Length);

        //    if (HasCheckSum)
        //        data = data.Substring(0, data.Length - 2);

        //    var result = new Dictionary<string, string>();

        //    // data = data.ToUpper()

        //    int index = 0;
        //    while (index < data.Length)
        //    {
        //        var ai = GetAI(data, ref index);
        //        if (ai is null)
        //        {
        //            if (throwException)
        //                throw new InvalidOperationException("AI not found");
        //            return result;
        //        }

        //        string code = GetCode(data, ai, ref index);
        //        result[ai.AI] = code;
        //    }

        //    return result;
        //}

        //private AII GetAI(string data, ref int index, bool usePlaceHolder = false)
        //{
        //    AII result = null;
        //    for (int i = minLengthOfAI, loopTo = maxLengthOfAI; i <= loopTo; i++)
        //    {
        //        string ai = data.Substring(index, i);
        //        if (usePlaceHolder)
        //            ai = ai.Remove(ai.Length - 1) + "d";
        //        if (aiiDict.TryGetValue(ai, out result))
        //        {
        //            index += i;
        //            return result;
        //        }
        //    }

        //    if (!usePlaceHolder)
        //        result = GetAI(data, ref index, true);
        //    return result;
        //}

        //private string GetCode(string data, AII ai, ref int index)
        //{
        //    int dataLength;
        //    if (ai.FNC1)
        //    {
        //        if (data.IndexOf(GroupSeperator, index) > 0)
        //        {
        //            dataLength = data.IndexOf(GroupSeperator, index) - index + 1;
        //        }
        //        else
        //        {
        //            dataLength = data.Length - index;
        //        }
        //    }
        //    else
        //    {
        //        dataLength = ai.LengthOfData;
        //        if (dataLength > data.Length - index)
        //        {
        //            dataLength = data.Length - index;
        //        }
        //    }

        //    string result = data.Substring(index, dataLength).Replace(Convert.ToString(GroupSeperator), string.Empty);
        //    index += dataLength;


        //    // Dim lengthToRead As Integer = Math.Min(ai.LengthOfData, data.Length - index)


        //    // Dim result As String = data.Substring(index, lengthToRead)

        //    // If ai.FNC1 Then

        //    // Dim indexOfGroupTermination As Integer = result.IndexOf(GroupSeperator)

        //    // If indexOfGroupTermination >= 0 Then
        //    // lengthToRead = indexOfGroupTermination + 1
        //    // Else
        //    // lengthToRead = lengthToRead + 1
        //    // End If


        //    // result = data.Substring(index, lengthToRead).Replace(GroupSeperator, String.Empty)

        //    // End If

        //    // index += lengthToRead

        //    return result;
        //}

        public SinglePackServices.G110Response VerifySinglePack(string gtin, string batchNumber, string expiryDate, string serialNumber, string nhrn = "",string schemaType="GTIN")
        {
            var g110 = new SinglePack.G110(certPath, certPassword);
            var body = GetSinglePackBody(gtin, batchNumber, expiryDate, serialNumber,nhrn,schemaType);

            sw.Start();
            SinglePackServices.G110Response t1 = g110.G110Verify(body, _singlePackEndpoint, clientId, userId, userPwd);
            sw.Stop();
            return t1;
        }

        public SinglePackServices.G140Response ExportSinglePack(string gtin, string batchNumber, string expiryDate, string serialNumber,string schemaType = "GTIN")
        {
            var g140 = new SinglePack.G140(certPath, certPassword);
            var body = GetSinglePackBody(gtin, batchNumber, expiryDate, serialNumber,"",schemaType);
            sw.Start();
            SinglePackServices.G140Response t1 = g140.G140Export(body, _singlePackEndpoint, clientId, userId, userPwd);
            sw.Stop();
            return t1;
        }

        public SinglePackServices.G141Response UndoExportSinglePack(string gtin, string batchNumber, string expiry, string serialNumber, string trxID = null,string scanType = "GTIN")
        {
            var g141 = new SinglePack.G141(certPath, certPassword);
            var body = GetSinglePackUndoExportRequestBody(gtin, batchNumber, expiry, serialNumber, trxID, scanType);
            sw.Start();
            SinglePackServices.G141Response t1 = g141.G141UndoExport(body, _singlePackEndpoint, clientId, userId, userPwd);
            sw.Stop();
            return t1;
        }

        private object GetSinglePackUndoExportRequestBody(string gtin, string batchNumber, string expiry, string serialNumber, string trxID = null,string scanType = "GTIN")
        {

            // Set the serial number of a single pack
            var pack = new SinglePackServices.RequestPack_Type();
            // pack.sn = "ABCDEFG"
            pack.sn = serialNumber;

            // Set the product code and the schema of a single pack
            var productCode = new SinglePackServices.ProductIdentifier_Type();
            // productCode.Value = "1234567891234"
            productCode.Value = gtin;
            if (scanType == "GTIN")
                productCode.scheme = SinglePackServices.CatalogProductSchema_Type.GTIN;
            else
                productCode.scheme = SinglePackServices.CatalogProductSchema_Type.PPN;
            // Alternativ PPN

            // Set batch id and expiration date
            var batch = new SinglePackServices.BaseBatch_Type();
            // batch.Id = "1"
            // batch.ExpDate = "170721"
            batch.Id = batchNumber;
            batch.ExpDate = expiry;

            // Append the productcode and batch objects to the RequestProduct_Type object
            var product = new SinglePackServices.RequestProduct_Type();
            product.ProductCode = productCode;
            product.Batch = batch;

            // Append the pack and product object to the RequestUndoSingleData_type object, and set the guid as RefClientTrxId
            var bodyUndo = new SinglePackServices.RequestUndoSingleData_Type();
            bodyUndo.Pack = pack;
            bodyUndo.Product = product;
            // Optinal: bodyUndo.RefClientTrxId = "06c8dea069c24446949e7806e028bcaf"; //Alternativ: Guid.NewGuid().ToString().Replace("-", "");
            if (!string.IsNullOrEmpty(trxID))
            {
                bodyUndo.RefClientTrxId = trxID;
            }

            return bodyUndo;
        }

        public SinglePackServices.G130Response DestroySinglePack(string gtin, string batchNumber, string expiryDate, string serialNumber,string scanType = "GTIN")
        {
            var g130 = new SinglePack.G130(certPath, certPassword);
            var body = GetSinglePackBody(gtin, batchNumber, expiryDate, serialNumber,"",scanType);
            SinglePackServices.G130Response t1 = g130.G130Destroy(body, _singlePackEndpoint, clientId, userId, userPwd);
            return t1;
        }

        public SinglePackServices.G180Response StolenSinglePack(string gtin, string batchNumber, string expiryDate, string serialNumber,string scanType = "GTIN")
        {
            var g180 = new SinglePack.G180(certPath, certPassword);
            var body = GetSinglePackBody(gtin, batchNumber, expiryDate, serialNumber,"",scanType);
            SinglePackServices.G180Response t1 = g180.G180Stolen(body, _singlePackEndpoint, clientId, userId, userPwd);
            return t1;
        }

        public SinglePackServices.G150Response SampleSinglePack(string gtin, string batchNumber, string expiryDate, string serialNumber, string scanType = "GTIN")
        {
            var g150 = new SinglePack.G150(certPath, certPassword);
            var body = GetSinglePackBody(gtin, batchNumber, expiryDate, serialNumber, "", scanType);
            sw.Start();
            SinglePackServices.G150Response t1 = g150.G150Sample(body, _singlePackEndpoint, clientId, userId, userPwd);
            sw.Stop();
            return t1;
        } 

        public SinglePackServices.G151Response UndoSampleSinglePack(string gtin, string batchNumber, string expiryDate, string serialNumber, string trxID = null,string scanType = "GTIN")
        {
            var g151 = new SinglePack.G151(certPath, certPassword);
            var body = GetSinglePackUndoSampleRequestBody(gtin, batchNumber, expiryDate, serialNumber, trxID,scanType);
            sw.Start();
            SinglePackServices.G151Response t1 = g151.G151UndoSample(body, _singlePackEndpoint, clientId, userId, userPwd);
            sw.Stop();
            return t1;
        }

        private object GetSinglePackUndoSampleRequestBody(string gtin, string batchNumber, string expiry, string serialNumber, string trxID = null,string scanType = "GTIN")
        {

            // Set the serial number of a single pack
            var pack = new SinglePackServices.RequestPack_Type();
            // pack.sn = "ABCDEFG"
            pack.sn = serialNumber;

            // Set the product code and the schema of a single pack
            var productCode = new SinglePackServices.ProductIdentifier_Type();
            // productCode.Value = "1234567891234"
            productCode.Value = gtin;

            if (scanType == "GTIN")
            productCode.scheme = SinglePackServices.CatalogProductSchema_Type.GTIN;
            else
            productCode.scheme = SinglePackServices.CatalogProductSchema_Type.PPN;

            // Alternativ PPN

            // Set batch id and expiration date
            var batch = new SinglePackServices.BaseBatch_Type();
            // batch.Id = "1"
            // batch.ExpDate = "170721"
            batch.Id = batchNumber;
            batch.ExpDate = expiry;

            // Append the productcode and batch objects to the RequestProduct_Type object
            var product = new SinglePackServices.RequestProduct_Type();
            product.ProductCode = productCode;
            product.Batch = batch;

            // Append the pack and product object to the RequestUndoSingleData_type object, and set the guid as RefClientTrxId
            var bodyUndo = new SinglePackServices.RequestUndoSingleData_Type();
            bodyUndo.Pack = pack;
            bodyUndo.Product = product;

            // bodyUndo.RefClientTrxId = "06c8dea069c24446949e7806e028bcaf"
            if (!string.IsNullOrEmpty(trxID))
            {
                bodyUndo.RefClientTrxId = trxID;
            }

            // Alternativ: Guid.NewGuid().ToString().Replace("-", "");

            return bodyUndo;
        }

        public SinglePackServices.G160Response FreeSampleSinglePack(string gtin, string batchNumber, string expiryDate, string serialNumber)
        {
            var g160 = new SinglePack.G160(certPath, certPassword);
            var body = GetSinglePackBody(gtin, batchNumber, expiryDate, serialNumber);
            sw.Start();
            SinglePackServices.G160Response t1 = g160.G160FreeSample(body, _singlePackEndpoint, clientId, userId, userPwd);
            sw.Stop();
            return t1;
        }

        public SinglePackServices.G161Response UndoFreeSampleSinglePack(string gtin, string batchNumber, string expiryDate, string serialNumber, string trxID = null)
        {
            var g161 = new SinglePack.G161(certPath, certPassword);
            var body = GetSinglePackUndoFreeSampleRequestBody(gtin, batchNumber, expiryDate, serialNumber, trxID);
            sw.Start();
            SinglePackServices.G161Response t1 = g161.G161UndoFreeSample(body, _singlePackEndpoint, clientId, userId, userPwd);
            sw.Stop();
            return t1;
        }

        private object GetSinglePackUndoFreeSampleRequestBody(string gtin, string batchNumber, string expiry, string serialNumber, string trxID = null)
        {
            // Set the serial number of a single pack
            var pack = new SinglePackServices.RequestPack_Type();
            // pack.sn = "ABCDEFG"
            pack.sn = serialNumber;

            // Set the product code and the schema of a single pack
            var productCode = new SinglePackServices.ProductIdentifier_Type();
            // productCode.Value = "1234567891234"
            productCode.Value = gtin;
            productCode.scheme = SinglePackServices.CatalogProductSchema_Type.GTIN;
            // Alternativ PPN

            // Set batch id and expiration date
            var batch = new SinglePackServices.BaseBatch_Type();
            // batch.Id = "1"
            // batch.ExpDate = "170721"
            batch.Id = batchNumber;
            batch.ExpDate = expiry;

            // Append the productcode and batch objects to the RequestProduct_Type object
            var product = new SinglePackServices.RequestProduct_Type();
            product.ProductCode = productCode;
            product.Batch = batch;

            // Append the pack and product object to the RequestUndoSingleData_type object, and set the guid as RefClientTrxId
            var bodyUndo = new SinglePackServices.RequestUndoSingleData_Type();
            bodyUndo.Pack = pack;
            bodyUndo.Product = product;
            if (!string.IsNullOrEmpty(trxID))
            {
                bodyUndo.RefClientTrxId = trxID;
            }
            // Optional: bodyUndo.RefClientTrxId = "06c8dea069c24446949e7806e028bcaf"; //Alternativ: Guid.NewGuid().ToString().Replace("-", "");

            return bodyUndo;
        }

        public SinglePackServices.G170Response LockSinglePack(string gtin, string batchNumber, string expiryDate, string serialNumber,string scanType = "GTIN")
        {
            var g170 = new SinglePack.G170(certPath, certPassword);
            var body = GetSinglePackBody(gtin, batchNumber, expiryDate, serialNumber,"",scanType);
            SinglePackServices.G170Response t1 = g170.G170Lock(body, _singlePackEndpoint, clientId, userId, userPwd);
            return t1;
        }

        public SinglePackServices.G171Response UndoLockSinglePack(string gtin, string batchNumber, string expiryDate, string serialNumber, string trxID = null)
        {
            var g171 = new SinglePack.G171(certPath, certPassword);
            var body = GetSinglePackUndoLockRequestBody(gtin, batchNumber, expiryDate, serialNumber, trxID);
            sw.Start();
            SinglePackServices.G171Response t1 = g171.G171UndoLock(body, _singlePackEndpoint, clientId, userId, userPwd);
            sw.Stop();
            return t1;
        }

        private object GetSinglePackUndoLockRequestBody(string gtin, string batchNumber, string expiry, string serialNumber, string trxID = null)
        {
            // Set the serial number of a single pack
            var pack = new SinglePackServices.RequestPack_Type();
            // pack.sn = "ABCDEFG"
            pack.sn = serialNumber;

            // Set the product code and the schema of a single pack
            var productCode = new SinglePackServices.ProductIdentifier_Type();
            // productCode.Value = "1234567891234"
            productCode.Value = gtin;
            productCode.scheme = SinglePackServices.CatalogProductSchema_Type.GTIN;
            // Alternativ PPN

            // Set batch id and expiration date
            var batch = new SinglePackServices.BaseBatch_Type();
            // batch.Id = "1"
            // batch.ExpDate = "170721"
            batch.Id = batchNumber;
            batch.ExpDate = expiry;

            // Append the productcode and batch objects to the RequestProduct_Type object
            var product = new SinglePackServices.RequestProduct_Type();
            product.ProductCode = productCode;
            product.Batch = batch;

            // Append the pack and product object to the RequestUndoSingleData_type object, and set the guid as RefClientTrxId
            var bodyUndo = new SinglePackServices.RequestUndoSingleData_Type();
            bodyUndo.Pack = pack;
            bodyUndo.Product = product;
            if (!string.IsNullOrEmpty(trxID))
            {
                bodyUndo.RefClientTrxId = trxID;
            }
            // Optional: bodyUndo.RefClientTrxId = "06c8dea069c24446949e7806e028bcaf"; //Alternativ: Guid.NewGuid().ToString().Replace("-", "");
            return bodyUndo;
        }

        public object GetSinglePackBody(string gtin, string batchNumber, 
            string expiryDate, string serialNumber,string nhrn = "", string schemaType = "GTIN")
        {
            var body = new SinglePackServices.RequestData_Type();
            var pack = new SinglePackServices.RequestPack_Type();
            pack.sn = serialNumber;

            // Set the product code and the schema of a single pack
            var productCode = new SinglePackServices.ProductIdentifier_Type();
            productCode.Value = gtin;

            if (schemaType == "GTIN")
            productCode.scheme = SinglePackServices.CatalogProductSchema_Type.GTIN;
            else
            productCode.scheme = SinglePackServices.CatalogProductSchema_Type.PPN;

            // Alternativ GTIN

            // Set batch id and expiration date
            var batch = new SinglePackServices.BaseBatch_Type();
            batch.Id = batchNumber;
            batch.ExpDate = expiryDate;

            // Append the productcode and batch objects to the RequestProduct_Type object
            var product = new SinglePackServices.RequestProduct_Type();
            product.ProductCode = productCode;
            product.Batch = batch;

            if (!string.IsNullOrEmpty(nhrn))
            {
                product.NHRN = nhrn;
            }

            // Append the pack and product object to the RequestData_Type object
            // Dim body As New SinglePackServices.RequestData_Type()
            body.Pack = pack;
            body.Product = product;

            return body;
        }

        private BulkServices.RequestProduct_Type GetBulkServicesProductType(string gtin, string batchNumber, string expiryDate)
        {
            var product = new BulkServices.RequestProduct_Type();
            product.ProductCode = new BulkServices.ProductIdentifier_Type();
            product.ProductCode.scheme = BulkServices.CatalogProductSchema_Type.GTIN;
            product.ProductCode.Value = gtin;
            product.Batch = new BulkServices.BaseBatch_Type();
            product.Batch.Id = batchNumber;
            product.Batch.ExpDate = expiryDate;
            return product;
        }

        private List<BulkServices.RequestPack_Type> GetBulkServicesPackType(List<string> serialNumbers)
        {
            var packs = new List<BulkServices.RequestPack_Type>();
            BulkServices.RequestPack_Type pack;
            for (int i = 0, loopTo = serialNumbers.Count - 1; i <= loopTo; i++)
            {
                pack = new BulkServices.RequestPack_Type();
                pack.sn = serialNumbers[i];
                packs.Add(pack);
            }

            return packs;
        }

        private object GetBulkBody(string gtin, string batchNumber, string expiryDate, List<string> serialNumbers)
        {
            var body = new BulkServices.RequestBulkData_Type();
            body.Product = GetBulkServicesProductType(gtin, batchNumber, expiryDate);
            body.Packs = GetBulkServicesPackType(serialNumbers).ToArray();
            return body;
        }

        public G188Response VerifyBox(string gtin, string batchNumber, string expiryDate, List<string> serialNumbers)
        {
            G115Response t1 = VerifyBulkPack(gtin, batchNumber, expiryDate, serialNumbers);
            G188Response t2 = VerifyBulkPackResult(t1.Header.Transaction.NMVSTrxId);
            return t2;
        }

        public BulkServices.G115Response VerifyBulkPack(string gtin, string batchNumber, string expiryDate, List<string> serialNumbers)
        {
            var body = GetBulkBody(gtin, batchNumber, expiryDate, serialNumbers);
            var g115 = new Bulk.G115(certPath, certPassword);
            sw.Start();
            BulkServices.G115Response response = g115.G115BulkVerify(body, BulkServicesEndPoint, clientId, userId, userPwd);
            sw.Stop();
            return response;
        }

        public BulkServices.G188Response VerifyBulkPackResult(string transID)
        {
            var body = GetBulkPackResultRequestBody(transID);
            var g188 = new Bulk.G188(certPath, certPassword);
            sw.Start();
            BulkServices.G188Response response = g188.G188RequestBulkPackResult(body, BulkServicesEndPoint, clientId, userId, userPwd);
            sw.Stop();
            return response;
        }

        private object GetBulkPackResultRequestBody(string transID)
        {
            // set the guid as RefClientTrxId, append to the I6_ResultTransactionData_Type object
            var body = new BulkServices.I6_ResultTransactionData_Type();
            // body.RefNMVSTrxId = "06c8dea069c24446949e7806e028bcaf"
            body.RefNMVSTrxId = transID;
            // Alternativ: Guid.NewGuid().ToString().Replace("-", "");
            return body;
        }

        private object GetSinglePackBodyUndo(string gtin, string batchNumber, string expiryDate, string serialNumber, string transID)
        {
            var bodyUndo = new SinglePackServices.RequestUndoSingleData_Type();
            bodyUndo.RefClientTrxId = transID;
            var pack = new SinglePackServices.RequestPack_Type();
            pack.sn = serialNumber;

            // Set the product code and the schema of a single pack
            var productCode = new SinglePackServices.ProductIdentifier_Type();
            productCode.Value = gtin;
            productCode.scheme = SinglePackServices.CatalogProductSchema_Type.GTIN;
            // Alternativ GTIN

            // Set batch id and expiration date
            var batch = new SinglePackServices.BaseBatch_Type();
            batch.Id = batchNumber;
            batch.ExpDate = expiryDate;

            // Append the productcode and batch objects to the RequestProduct_Type object
            var product = new SinglePackServices.RequestProduct_Type();
            product.ProductCode = productCode;
            product.Batch = batch;

            // Append the pack and product object to the RequestData_Type object
            // Dim body As New SinglePackServices.RequestData_Type()
            bodyUndo.Pack = pack;
            bodyUndo.Product = product;
            return bodyUndo;
        }

        private void InitConfig()
        {

            // certPath = context.Server.MapPath("NMVS\LILE1001.p12")
            // certPassword = "EbJrXQPn"
            // clientId = "SWS"
            // userId = "LILE1001"
            // userPwd = "Letmein@60"
            // singlePackEndpoint = "https://ws-single-transactions-int-bp.nmvs.eu:8443/WS_SINGLE_TRANSACTIONS_V1/SinglePackServiceV20"
            // bulkServicesEndPoint = "https://ws-bulk-transactions-int-bp.nmvs.eu:8445/WS_BULK_TRANSACTIONS_V1/BulkServiceV20"

            if (sw is null)
            {
                sw = new Stopwatch();
            }

            string mapPath = context.Server.MapPath(string.Empty).Replace(@"\api", @"\Content");
            switch (CompanyID)
            {
                case 0:
                    {

                        // certPath = context.Server.MapPath("NMVS\LILE1001.p12")
                        certPath = string.Format(@"{0}\{1}", mapPath, @"NMVS\LILE1001.p12");
                        certPassword = "EbJrXQPn";
                        clientId = "SWS";
                        userId = "LILE1001";
                        userPwd = "Letmein@60";
                        break;
                    }

                case 6: // cst
                    {

                        // certPath = context.Server.MapPath("NMVS\CST\41119253.p12")
                        certPath = string.Format(@"{0}\{1}", mapPath, @"NMVS\CST\41119253.p12");
                        // jcl 22/10/2020
                        // certPassword = "7d7vrtcs"
                        certPassword = "vfXdTnN1";
                        clientId = "WHOLUK";
                        userId = "41119253";
                        // userPwd = "Cpcm35n-r"
                        userPwd = "jgWue4UqW9DSj8i~";
                        break;
                    }

                case 21: // dmg
                    {
                        // certPath = context.Server.MapPath("NMVS\DMG\92121324.p12")
                        certPath = string.Format(@"{0}\{1}", mapPath, @"NMVS\DMG\92121324.p12");
                        // jcl 22/10/2020
                        // certPassword = "2wq42rJn"
                        certPassword = "TJGxrFNm";
                        clientId = "WHOLUK";
                        userId = "92121324";
                        // userPwd = "g8+shFu%2"
                        userPwd = "DNJw8WQq8cyqKwB~";
                        break;
                    }

                case 22: // acre
                    {
                        // certPath = context.Server.MapPath("NMVS\ACRE\14586903.p12")
                        certPath = string.Format(@"{0}\{1}", mapPath, @"NMVS\ACRE\14586903.p12");
                        // jcl 22/10/2020
                        // certPassword = "utJ8JJ4e"
                        certPassword = "Wmrw1Sph";
                        clientId = "WHOLUK";
                        userId = "14586903";
                        // userPwd = "7BVYj#-_%"
                        userPwd = "FEBthaTLwh99KwF~";
                        break;
                    }

                case 26: // acer
                    {
                        // certPath = context.Server.MapPath("NMVS\ACER\38189716.p12")
                        certPath = string.Format(@"{0}\{1}", mapPath, @"NMVS\ACER\38189716.p12");
                        // jcl 22/10/2020
                        // certPassword = "uEHCRcdm"
                        certPassword = "RNAe9SE8";
                        clientId = "WHOLUK";
                        userId = "38189716";
                        // userPwd = "E9BRf+9kC"
                        userPwd = "Qe7XQebaCG6eFLg~";
                        break;
                    }

                case 44: // saima
                    {
                        // Me.GroupSeperator = ChrW(38)
                        certPath = string.Format(@"{0}\{1}", mapPath, @"NMVS\SGF\7USDIS350014949.p12");
                        //certPassword = "SpTAeq4v";
                        //certPassword = "56pVEppB";
                        certPassword = "ek1ShEKA"; // updated by jcl 21/6/2023
                        clientId = "7GRDISCATALUNYA";
                        userId = "7USDIS350014949";
                        // userPwd = "F+j7q6$X"
                        // userPwd = "XMidhiUqvr2901!"
                        //userPwd = "nTZ%f3Esw#8A";
                        //userPwd = "n0o#jCiX2HT!"; //updated by jcl 11/10/2021
                        //userPwd = "axH^3#T4#q8a"; //updated by jcl 11/10/2022
                        userPwd = "#qYY0Dxzhi1!"; //updated by jcl 15/10/2023
                        break;
                    }

                case 144: // saima test
                    {
                        // Me.GroupSeperator = ChrW(38)
                        certPath = string.Format(@"{0}\{1}", mapPath, @"NMVS\SGF\7USDIS350014949.p12");
                        //certPassword = "SpTAeq4v";
                        certPassword = "ek1ShEKA"; // updated by jcl 21/6/2023
                        clientId = "7GRDISCATALUNYA";
                        userId = "7USDIS350014949";
                        // userPwd = "F+j7q6$X"
                        // userPwd = "XMidhiUqvr2901!"
                        //userPwd = "nTZ%f3Esw#8A";
                        //userPwd = "n0o#jCiX2HT!"; //updated by jcl 11/10/2021
                        userPwd = "#qYY0Dxzhi1!"; //updated by jcl 15/10/2023

                        break;
                    }


            }

            if (CompanyID > 0)
            {
                if (CompanyID == 44 || CompanyID == 144)
                {
                    // singlePackEndpoint = "https://ws-single-transactions-prod-es.nmvs.eu:8443/WS_SINGLE_TRANSACTIONS_V1/SinglePackServiceV20"
                    // bulkServicesEndPoint = "https://ws-bulk-transactions-prod-es.nmvs.eu:8445/WS_BULK_TRANSACTIONS_V1/BulkServiceV20"
                    //_singlePackEndpoint = "https://ws-single-transactions-prod-es.nmvs.eu:8443/WS_SINGLE_TRANSACTIONS_V1/SinglePackServiceV30";
                    //BulkServicesEndPoint = "https://ws-bulk-transactions-prod-es.nmvs.eu:8445/WS_BULK_TRANSACTIONS_V1/BulkServiceV30";
                    _singlePackEndpoint = "https://ws-single-transactions-prod-es.nmvs.eu:8443/WS_SINGLE_TRANSACTIONS_V1/SinglePackServiceV50";
                    BulkServicesEndPoint = "https://ws-bulk-transactions-prod-es.nmvs.eu:8445/WS_BULK_TRANSACTIONS_V1/BulkServiceV50";                
                }
                else
                {
                    // singlePackEndpoint = "https://ws-single-transactions-prod-uk.nmvs.eu:8443/WS_SINGLE_TRANSACTIONS_V1/SinglePackServiceV20"
                    // bulkServicesEndPoint = "https://ws-bulk-transactions-prod-uk.nmvs.eu:8445/WS_BULK_TRANSACTIONS_V1/BulkServiceV20"
                    _singlePackEndpoint = "https://ws-single-transactions-prod-uk.nmvs.eu:8443/WS_SINGLE_TRANSACTIONS_V1/SinglePackServiceV30";
                    BulkServicesEndPoint = "https://ws-bulk-transactions-prod-uk.nmvs.eu:8445/WS_BULK_TRANSACTIONS_V1/BulkServiceV30";
                }
            }

            // singlePackEndpoint = "https://ws-single-transactions-prod-uk.nmvs.eu:8443/WS_SINGLE_TRANSACTIONS_V1/SinglePackServiceV30"
            // bulkServicesEndPoint = "https://ws-bulk-transactions-prod-uk.nmvs.eu:8445/WS_BULK_TRANSACTIONS_V1/BulkServiceV30"

            else
            {
                _singlePackEndpoint = "https://ws-single-transactions-int-bp.nmvs.eu:8443/WS_SINGLE_TRANSACTIONS_V1/SinglePackServiceV20";
                BulkServicesEndPoint = "https://ws-bulk-transactions-int-bp.nmvs.eu:8445/WS_BULK_TRANSACTIONS_V1/BulkServiceV20";
            }
        }

        public DateTime GetDateFromString(string expiryDate)
        {
            int yy = int.Parse(expiryDate.Substring(0, 2)) + 2000;
            int mm = int.Parse(expiryDate.Substring(2, 2));
            int dd = int.Parse(expiryDate.Substring(4, 2));
            DateTime dt;
            if (dd > 0)
            {
                dt = new DateTime(yy, mm, dd);
            }
            else
            {
                if (mm == 12)
                {
                    yy = yy + 1;
                    mm = 1;
                }
                else
                {
                    mm = mm + 1;
                }

                dt = new DateTime(yy, mm, 1).AddDays(-1);
            }

            return dt;
        }

        private bool _isValid;

        public bool IsValid
        {
            get
            {
                return _isValid;
            }
        }

        public UIDInfo GetUIDInfo(string barcode)
        {

            _isValid = true;

            return uidHelper.GetUIDInfo(barcode);

            //if (string.IsNullOrEmpty(barcode))
            //{
            //    _isValid = false;
            //    return null;
            //}

            //Dictionary<string, string> results;
            //string uid = barcode.Trim();
            //try
            //{
            //    results = Parse(uid, true);
            //}
            //catch (Exception ex)
            //{
            //    _isValid = false;
            //    return null;
            //}

            //var retval = new UIDInfo();
            
            //if (results.ContainsKey("01"))
            //retval.GTIN = results["01"];

            //if (results.ContainsKey("9N"))
            //    retval.PPN = results["9N"];

            //if (results.ContainsKey("8P"))
            //    retval.GTIN = results["8P"];

            //if (results.ContainsKey("10"))
            //retval.BatchNumber = results["10"];

            //if (results.ContainsKey("1T"))
            //    retval.BatchNumber = results["1T"];

            //if (results.ContainsKey("17"))
            //retval.Expiry = results["17"];

            //if (results.ContainsKey("D"))
            //    retval.Expiry = results["D"];

            //retval.ExpiryDate = GetDateFromString(retval.Expiry);

            //if (results.ContainsKey("21"))
            //retval.SerialNumber = results["21"];

            //if (results.ContainsKey("S"))
            //    retval.SerialNumber = results["S"];

            //if (results.ContainsKey("712"))
            //retval.NHRN = results["712"];
            
            //retval.Uid = uid;
            
            //return retval;

        }

        public Dictionary<string, string> Parse(string uid,bool throwException = false)
        {
            UIDHelper uidHelper = new UIDHelper();
            return uidHelper.Parse(uid, throwException);
        }

        public string GetUIDClientInfo(string barcode)
        {

            return uidHelper.GetUIDClientInfo(barcode);

            //UIDInfo info = null;

            //info = GetUIDInfo(barcode);

            //if (info is object)
            //{
            //    var js = new JavaScriptSerializer();
            //    return js.Serialize(info);
            //}
            //else
            //{
            //    return null;
            //}

        }



    }
}
