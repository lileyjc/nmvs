﻿using System;
using System.ServiceModel;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using Message = System.ServiceModel.Channels.Message;


namespace Evolution.NMVS
{
    class Program
    {
        static void Main(string[] args)
        {
            /*!!
             * The code is written as simple as possible, which means that OOP was omitted if possible. 
             * This is intended to ensure that the individual functions can be quickly and easily understood 
             * and reprocessed.
             *!! 
            */

            //Use at least.Net Framework 4.5
            //add using directive System;

            /* Step 1: Include the WSDL files for the Web services
             * In Solution Explorer, right-click the name of the project that you want to add the service to, and then click Add and than Service Reference.
             * The Add Service Reference dialog box appears.
             * Add the local path to your wsdl files in the address bar. For Example: C:\WebServices\wsdl\WS_SINGLE_PACK.wsdl
             * Enter a meaningful name under Namespace. For Example SinglePackServices
             * Click Ok.
             * Use the following WSDL files: -WS_BULK -WS_Master_Data -WS_MIXED_BULK -WS_PKI -WS_SINGLE_PACK -WS_SUPPORT
            */

            /*
             * Security configuration:
             * - Use TLS 1.2
             */
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            //First choice: Service
            Console.WriteLine("1. Bulk");
            Console.WriteLine("2. MasterData");
            Console.WriteLine("3. MixedBulk");
            Console.WriteLine("4. Pki");
            Console.WriteLine("5. SinglePack");
            Console.WriteLine("6. Support");

            //Input query
            Console.Write("Insert Key here:");
            //Read the service selection
            string keyInput = Console.ReadLine();
            int keyIntInput;
            //Integers are passed as entered, chars are passed as 0
            Int32.TryParse(keyInput, out keyIntInput);

            //Input of the first selection is processed here
            switch (keyIntInput)
            {
                case 1:
                    //Second choice: Pack
                    Console.Clear();
                    Console.WriteLine(" 1. G115BulkVerify");
                    Console.WriteLine(" 2. G125BulkDispense");
                    Console.WriteLine(" 3. G127BulkUndoDispense");
                    Console.WriteLine(" 4. G135BulkDestroy");
                    Console.WriteLine(" 5. G145BulkExport");
                    Console.WriteLine(" 6. G147BulkUndoExport");
                    Console.WriteLine(" 7. G155BulkSample");
                    Console.WriteLine(" 8. G157BulkUndoSample");
                    Console.WriteLine(" 9. G165BulkFreeSample");
                    Console.WriteLine("10. G167BulkUndoFreeSample");
                    Console.WriteLine("11. G175BulkLocks");
                    Console.WriteLine("12. G177BulkUndoLock");
                    Console.WriteLine("13. G185BulkStolen");
                    Console.WriteLine("14. G188RequestBulkPackResult");
                    Console.WriteLine("15. G199RequestPickupIdsForBulkTransactionResult");
                    Console.WriteLine("16. PingBulk");
                    Console.Write("Insert Key here:");

                    keyInput = Console.ReadLine();
                    Int32.TryParse(keyInput, out keyIntInput);

                    //Input of the second selection is processed here
                    BulkChoice(keyIntInput);
                    break;
                case 2:
                    Console.Clear();
                    Console.WriteLine(" 1. G101DownloadProductMasterData");
                    Console.WriteLine(" 2. PingMasterData"); 
                    Console.Write("Insert Key here:");

                    keyInput = Console.ReadLine();
                    Int32.TryParse(keyInput, out keyIntInput);

                    //Input of the second selection is processed here
                    MasterDataChoice(keyIntInput);
                    break;
                case 3:
                    Console.Clear();
                    Console.WriteLine(" 1. G195SubmitBulkTransaction");
                    Console.WriteLine(" 2. G196RequestBulkTransactionResult");
                    Console.WriteLine(" 3. PingMixedBulk");
                    Console.Write("Insert Key here:");

                    keyInput = Console.ReadLine();
                    Int32.TryParse(keyInput, out keyIntInput);

                    //Input of the second selection is processed here
                    MixedBulkChoice(keyIntInput);
                    break;
                case 4:
                    Console.Clear();
                    Console.WriteLine("1. G615DownloadClientCertificate");
                    Console.WriteLine("2. PingPki");
                    Console.Write("Insert Key here:");

                    keyInput = Console.ReadLine();
                    Int32.TryParse(keyInput, out keyIntInput);

                    //Input of the second selection is processed here
                    PkiChoice(keyIntInput);
                    break;
                case 5:
                    Console.Clear();
                    Console.WriteLine(" 1. G110Verify");
                    Console.WriteLine(" 2. G112ManualVerify");
                    Console.WriteLine(" 3. G120Dispense");
                    Console.WriteLine(" 4. G121UndoDispense");
                    Console.WriteLine(" 5. G122DispenseManualEntry");
                    Console.WriteLine(" 6. G123UndoDispenseManualEntry");
                    Console.WriteLine(" 7. G130Destroy");
                    Console.WriteLine(" 8. G140Export");
                    Console.WriteLine(" 9. G141UndoExport");
                    Console.WriteLine("10. G150Sample");
                    Console.WriteLine("11. G151UndoSample");
                    Console.WriteLine("12. G160FreeSample");
                    Console.WriteLine("13. G161UndoFreeSample");
                    Console.WriteLine("14. G170Lock");
                    Console.WriteLine("15. G171UndoLock");
                    Console.WriteLine("16. G180Stolen");
                    Console.WriteLine("17. G182StolenManualEntry");
                    Console.WriteLine("18. PingSinglePack");
                    Console.Write("Insert Key here:");

                    keyInput = Console.ReadLine();
                    Int32.TryParse(keyInput, out keyIntInput);

                    //Input of the second selection is processed here
                    SinglePackChoice(keyIntInput);
                    break;
                case 6:
                    Console.Clear();
                    Console.WriteLine("1. G445ChangePassword");
                    Console.WriteLine("2. G482LoadTermsAndConditions");
                    Console.WriteLine("3. G483ConfirmTermsAndConditions");
                    Console.WriteLine("4. G487LoadDataPrivacyPolicies");
                    Console.WriteLine("5. PingSupport");
                    Console.Write("Insert Key here:");

                    keyInput = Console.ReadLine();
                    Int32.TryParse(keyInput, out keyIntInput);

                    //Input of the second selection is processed here
                    SupportChoice(keyIntInput);
                    break;
                default:
                    return;
                    
            }
            Console.WriteLine("\nPress any key to exit the program:");
            Console.ReadKey();
        }

        /// <summary>
        /// The second input is processed here if the BulkService was selected during the first selection
        /// </summary>
        /// <param name="keyInput">Integer of the second input</param>
        public static void BulkChoice(int keyInput)
        {
            switch (keyInput)
            {
                case 1:
                    //Bulk.G115.G115BulkVerify();
                    break;
                case 2:
                    Bulk.G125.G125BulkDispense();
                    break;
                case 3:
                    Bulk.G127.G127BulkUndoDispense();
                    break;
                case 4:
                    Bulk.G135.G135BulkDestroy();
                    break;
                case 5:
                    Bulk.G145.G145BulkExport();
                    break;
                case 6:
                    Bulk.G147.G147BulkUndoExport();
                    break;
                case 7:
                    Bulk.G155.G155BulkSample();
                    break;
                case 8:
                    Bulk.G157.G157BulkUndoSample();
                    break;
                case 9:
                    Bulk.G165.G165BulkFreeSample();
                    break;
                case 10:
                    Bulk.G167.G167BulkUndoFreeSample();
                    break;
                case 11:
                    Bulk.G175.G175BulkLocks();
                    break;
                case 12:
                    Bulk.G177.G177BulkUndoLock();
                    break;
                case 13:
                    Bulk.G185.G185BulkStolen();
                    break;
                case 14:
                    //Bulk.G188.G188RequestBulkPackResult();
                    break;
                case 15:
                    Bulk.G199.G199RequestPickupIdsForBulkTransactionResult();
                    break;
                case 16:
                    Bulk.BulkPing.PingBulk();
                    break;
                default:
                    Console.WriteLine("Wrong number!");                    
                    return;
            }
        }
        /// <summary>
        /// The second input is processed here if the MasterDataService was selected during the first selection
        /// </summary>
        /// <param name="keyInput">Integer of the second input</param>
        public static void MasterDataChoice(int keyInput)
        {
            switch (keyInput)
            {
                case 1:
                    MasterData.G101.G101DownloadProductMasterData();
                    break;
                case 2:
                    MasterData.MasterDataPing.PingMasterData();
                    break;

                default:
                    Console.WriteLine("Wrong number!");
                    return;
            }
        }
        /// <summary>
        /// The second input is processed here if the MixedBulkService was selected during the first selection
        /// </summary>
        /// <param name="keyInput">Integer of the second input</param>
        public static void MixedBulkChoice(int keyInput)
        {
            switch (keyInput)
            {
                case 1:
                    MixedBulk.G195.G195SubmitBulkTransaction();
                    break;
                case 2:
                    MixedBulk.G196.G196RequestBulkTransactionResult();
                    break;
                case 3:
                    MixedBulk.MixedBulkPing.PingMixedBulk();
                    break;

                default:
                    Console.WriteLine("Wrong number!");
                    return;
            }
        }
        /// <summary>
        /// The second input is processed here if the PkiService was selected during the first selection
        /// </summary>
        /// <param name="keyInput">Integer of the second input</param>
        public static void PkiChoice(int keyInput)
        {
            switch (keyInput)
            {
                case 1:
                    Pki.G615.G615DownloadClientCertificate();
                    break;
                case 2:
                    Pki.PkiPing.PingPki();
                    break;                
                default:
                    Console.WriteLine("Wrong number!");
                    return;
            }
        }
        /// <summary>
        /// The second input is processed here if the SinglePackService was selected during the first selection
        /// </summary>
        /// <param name="keyInput">Integer of the second input</param>
        public static void SinglePackChoice(int keyInput)
        {
            switch (keyInput)
            {
                case 1:
                    //SinglePack.G110.G110Verify();
                    break;
                case 2:
                    SinglePack.G112.G112ManualVerify();
                    break;
                case 3:
                    SinglePack.G120.G120Dispense();
                    break;
                case 4:
                    SinglePack.G121.G121UndoDispense();
                    break;
                case 5:
                    SinglePack.G122.G122DispenseManualEntry();
                    break;
                case 6:
                    SinglePack.G123.G123UndoDispenseManualEntry();
                    break;
                case 7:
                    //SinglePack.G130.G130Destroy();
                    break;
                case 8:
                    //SinglePack.G140.G140Export();
                    break;
                case 9:
                    //SinglePack.G141.G141UndoExport();
                    break;
                case 10:
                    //SinglePack.G150.G150Sample();
                    break;
                case 11:
                    //SinglePack.G151.G151UndoSample();
                    break;
                case 12:
                    //SinglePack.G160.G160FreeSample();
                    break;
                case 13:
                    //SinglePack.G161.G161UndoFreeSample();
                    break;
                case 14:
                    //SinglePack.G170.G170Lock();
                    break;
                case 15:
                    //SinglePack.G171.G171UndoLock();
                    break;
                case 16:
                    //SinglePack.G180.G180Stolen();
                    break;
                case 17:
                    SinglePack.G182.G182StolenManualEntry();
                    break;
                case 18:
                    SinglePack.SinglePackPing.PingSinglePack();
                    break;
                default:
                    Console.WriteLine("Wrong number!");
                    return;
            }
        }
        /// <summary>
        /// The second input is processed here if the SupportService was selected during the first selection
        /// </summary>
        /// <param name="keyInput">Integer of the second input</param>
        public static void SupportChoice(int keyInput)
        {
            switch (keyInput)
            {
                case 1:
                    Support.G445.G445ChangePassword();
                    break;
                case 2:
                    Support.G482.G482LoadTermsAndConditions();
                    break;
                case 3:
                    Support.G483.G483ConfirmTermsAndConditions();
                    break;
                case 4:
                    Support.G487.G487LoadDataPrivacyPolicies();
                    break;
                case 5:
                    Support.SupportPing.PingSupport();
                    break;                
                default:
                    Console.WriteLine("Wrong number!");
                    return;
            }
        }
    }

    //add using directive System.ServiceModel.Description;
    //add using directive System.ServiceModel.Channels;
    //add using directive System.ServiceModel.Dispatcher;
    //add using directive System.ServiceModel;
    internal class CustomMessageInspector : IEndpointBehavior, IClientMessageInspector
    {        
        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(this);
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        //add using directive Message = System.ServiceModel.Channels.Message;
        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            // WORKAROUND for WCF-Exception "The MessageHeader is already understood" 
            // (Note: The message still gets validated)
            reply.Headers.Clear();
            Console.WriteLine("received Response:");
            Console.WriteLine("{0}\r\n", reply);
        }

        /// <summary>
        /// Shows the sent message with and without SOAP-Header
        /// </summary>
        /// <param name="request"></param>
        /// <param name="channel"></param>
        /// <returns></returns>
        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            Console.WriteLine("original Request:");
            Console.WriteLine("{0}\r\n", request);
            request.Headers.Clear();
            Console.WriteLine("without Header Request:");
            Console.WriteLine("{0}\r\n", request);
            return null;
        }        
    }
}
