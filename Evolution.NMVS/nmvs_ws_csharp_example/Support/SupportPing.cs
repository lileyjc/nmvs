﻿using System;
using Evolution.NMVS.SupportServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;

namespace Evolution.NMVS.Support
{
    class SupportPing
    {
        /// <summary>
        /// Ping request to test the connection to the Support-Services.
        /// Displays the XML request and the response.
        /// </summary>
        public static void PingSupport()
        {
            //add using directive Evolution.NMVS.SupportServices;
            //binding the service address you want to contact
            string EndPoint = "";

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            SupportServicesClient service = new SupportServicesClient(binding, new EndpointAddress(EndPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2("", "", X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates PingRequest and set the ping Input
            SupportPingRequest ping = new SupportPingRequest();
            ping.Input = "Hello World!";

            //Creates PingResponse, result of the request.
            SupportPingResponse pingResponse = service.PingSupport(ping);

            //Displays the response. If the request is successful, the output is equal to the input
            Console.WriteLine(pingResponse.Output);
        }
    }
}
