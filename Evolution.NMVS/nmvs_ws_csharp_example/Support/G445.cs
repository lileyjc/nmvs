﻿using System;
using Evolution.NMVS.SupportServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;

namespace Evolution.NMVS.Support
{
    class G445
    {
        public static object Header { get; set; }

        /// <summary>
        /// Creates a support request to change the password
        /// </summary>
        public static void G445ChangePassword()
        {
            //add using directive Evolution.NMVS.SupportServices;
            //binding the service address you want to contact
            string EndPoint = "";

            //Creates a G445Request object and a G445ChangePasswordRequest object
            G445ChangePasswordRequest request = new G445ChangePasswordRequest();
            G445Request g445Request = new G445Request();

            //Executes the SetHeaderRequest method from the header class to fill the header
            object Header = Evolution.NMVS.Header.SetHeaderRequest("Evolution.NMVS.SupportServices", false);
            //Executes the SetSupportRequestBody method to fill the body
            object Body = SetSupportRequestBody();

            //Appends the header and body to the request object
            g445Request.Header = (RequestHeaderData_Type)Header;
            g445Request.Body = (ChangePwdRequestData_Type)Body;
            request.G445Request = g445Request;

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            SupportServicesClient service = new SupportServicesClient(binding, new EndpointAddress(EndPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2("", "", X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates G445Response, result of the request.
            G445Response response = service.G445ChangePassword(g445Request);

            //Displays the response header and body.
            Console.WriteLine(response.Header);
            Console.WriteLine(response.Body);
        }

        /// <summary>
        /// Method to generate the body for a password change
        /// </summary>
        /// <returns></returns>
        private static object SetSupportRequestBody()
        {
            SupportServices.ChangePwdRequestData_Type body = new SupportServices.ChangePwdRequestData_Type();
            body.Password = "[Enter password here]";
            //Changing the password with the current password should result in an error. If the new password was used within the last 20 used, the password is not changed.
            body.NewPassword = "[Enter new password here]";
            return body;
        }
    }
}
