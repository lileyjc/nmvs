﻿using System;
using Evolution.NMVS.SupportServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;

namespace Evolution.NMVS.Support
{
    class G487
    {
        public static object Header { get; set; }

        /// <summary>
        /// Creates a support request to change the password
        /// </summary>
        public static void G487LoadDataPrivacyPolicies()
        {
            //add using directive Evolution.NMVS.SupportServices;
            //binding the service address you want to contact
            string EndPoint = "";

            //Creates a request object
            G487Request g487Request = new G487Request();

            //Executes the SetHeaderRequest method from the header class to fill the header
            object Header = Evolution.NMVS.Header.SetHeaderRequest("Evolution.NMVS.SupportServices", false);

            //Appends the header and body to the request object
            g487Request.Header = (RequestHeaderData_Type)Header;

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            SupportServicesClient service = new SupportServicesClient(binding, new EndpointAddress(EndPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2("", "", X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates response, result of the request.
            O12LoadDataPrivacyPolicyType response = service.G487LoadDataPrivacyPolicies(g487Request);

            //Displays the response header and body.
            Console.WriteLine(response.Header);
            Console.WriteLine(response.Body);
        }
    }
}
