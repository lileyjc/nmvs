﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Evolution.NMVS
{
    //public partial class UIDInfo
    //{
    //    public string PPN;
    //    public string GTIN;
    //    public string BatchNumber;
    //    public string Expiry;
    //    public DateTime ExpiryDate;
    //    public string SerialNumber;
    //    public string NHRN;
    //    public string Uid;
    //}

    public partial class UIDHelper
    {

        public enum DataType
        {
            Numeric,
            Alphanumeric
        }

        public partial class AII
        {
            public string AI { get; set; }
            public string Description { get; set; }
            public int LengthOfAI { get; set; }
            public DataType DataDescription { get; set; }
            public int LengthOfData { get; set; }
            public bool FNC1 { get; set; }

            public AII(string AI, string Description, int LengthOfAI, DataType DataDescription, int LengthOfData, bool FNC1)
            {
                this.AI = AI;
                this.Description = Description;
                this.LengthOfAI = LengthOfAI;
                this.DataDescription = DataDescription;
                this.LengthOfData = LengthOfData;
                this.FNC1 = FNC1;
            }

            public override string ToString()
            {
                return string.Format("{0} [{1}]", AI, Description);
            }
        }

        public UIDHelper()
        {

            aiiDict = new SortedDictionary<string, AII>();
            HasCheckSum = false;
            GroupSeperator = '^';
            EAN128StartCode = "]C1";
            ASCStartCode = "`=:06&";
            minLengthOfAI = 1;
            maxLengthOfAI = 4;

            Add("00", "SerialShippingContainerCode", 2, DataType.Numeric, 18, false);
            Add("01", "EAN-NumberOfTradingUnit", 2, DataType.Numeric, 14, false);
            Add("02", "EAN-NumberOfTheWaresInTheShippingUnit", 2, DataType.Numeric, 14, false);
            Add("10", "Batch_Number", 2, DataType.Alphanumeric, 20, true);
            Add("11", "ProducerDate_JJMMDD", 2, DataType.Numeric, 6, false);
            Add("12", "DueDate_JJMMDD", 2, DataType.Numeric, 6, false);
            Add("13", "PackingDate_JJMMDD", 2, DataType.Numeric, 6, false);
            Add("15", "MinimumDurabilityDate_JJMMDD", 2, DataType.Numeric, 6, false);
            Add("17", "ExpiryDate_JJMMDD", 2, DataType.Numeric, 6, false);
            Add("20", "ProductModel", 2, DataType.Numeric, 2, false);
            Add("21", "SerialNumber", 2, DataType.Alphanumeric, 20, true);
            Add("22", "HIBCCNumber", 2, DataType.Alphanumeric, 29, false);
            Add("240", "PruductIdentificationOfProducer", 3, DataType.Alphanumeric, 30, true);
            Add("241", "CustomerPartsNumber", 3, DataType.Alphanumeric, 30, true);
            Add("250", "SerialNumberOfAIntegratedModule", 3, DataType.Alphanumeric, 30, true);
            Add("251", "ReferenceToTheBasisUnit", 3, DataType.Alphanumeric, 30, true);
            Add("252", "GlobalIdentifierSerialisedForTrade", 3, DataType.Numeric, 2, false);
            Add("30", "AmountInParts", 2, DataType.Numeric, 8, true);
            Add("310d", "NetWeight_Kilogram", 4, DataType.Numeric, 6, false);
            Add("311d", "Length_Meter", 4, DataType.Numeric, 6, false);
            Add("312d", "Width_Meter", 4, DataType.Numeric, 6, false);
            Add("313d", "Heigth_Meter", 4, DataType.Numeric, 6, false);
            Add("314d", "Surface_SquareMeter", 4, DataType.Numeric, 6, false);
            Add("315d", "NetVolume_Liters", 4, DataType.Numeric, 6, false);
            Add("316d", "NetVolume_CubicMeters", 4, DataType.Numeric, 6, false);
            Add("320d", "NetWeight_Pounds", 4, DataType.Numeric, 6, false);
            Add("321d", "Length_Inches", 4, DataType.Numeric, 6, false);
            Add("322d", "Length_Feet", 4, DataType.Numeric, 6, false);
            Add("323d", "Length_Yards", 4, DataType.Numeric, 6, false);
            Add("324d", "Width_Inches", 4, DataType.Numeric, 6, false);
            Add("325d", "Width_Feed", 4, DataType.Numeric, 6, false);
            Add("326d", "Width_Yards", 4, DataType.Numeric, 6, false);
            Add("327d", "Heigth_Inches", 4, DataType.Numeric, 6, false);
            Add("328d", "Heigth_Feed", 4, DataType.Numeric, 6, false);
            Add("329d", "Heigth_Yards", 4, DataType.Numeric, 6, false);
            Add("330d", "GrossWeight_Kilogram", 4, DataType.Numeric, 6, false);
            Add("331d", "Length_Meter", 4, DataType.Numeric, 6, false);
            Add("332d", "Width_Meter", 4, DataType.Numeric, 6, false);
            Add("333d", "Heigth_Meter", 4, DataType.Numeric, 6, false);
            Add("334d", "Surface_SquareMeter", 4, DataType.Numeric, 6, false);
            Add("335d", "GrossVolume_Liters", 4, DataType.Numeric, 6, false);
            Add("336d", "GrossVolume_CubicMeters", 4, DataType.Numeric, 6, false);
            Add("337d", "KilogramPerSquareMeter", 4, DataType.Numeric, 6, false);
            Add("340d", "GrossWeight_Pounds", 4, DataType.Numeric, 6, false);
            Add("341d", "Length_Inches", 4, DataType.Numeric, 6, false);
            Add("342d", "Length_Feet", 4, DataType.Numeric, 6, false);
            Add("343d", "Length_Yards", 4, DataType.Numeric, 6, false);
            Add("344d", "Width_Inches", 4, DataType.Numeric, 6, false);
            Add("345d", "Width_Feed", 4, DataType.Numeric, 6, false);
            Add("346d", "Width_Yards", 4, DataType.Numeric, 6, false);
            Add("347d", "Heigth_Inches", 4, DataType.Numeric, 6, false);
            Add("348d", "Heigth_Feed", 4, DataType.Numeric, 6, false);
            Add("349d", "Heigth_Yards", 4, DataType.Numeric, 6, false);
            Add("350d", "Surface_SquareInches", 4, DataType.Numeric, 6, false);
            Add("351d", "Surface_SquareFeet", 4, DataType.Numeric, 6, false);
            Add("352d", "Surface_SquareYards", 4, DataType.Numeric, 6, false);
            Add("353d", "Surface_SquareInches", 4, DataType.Numeric, 6, false);
            Add("354d", "Surface_SquareFeed", 4, DataType.Numeric, 6, false);
            Add("355d", "Surface_SquareYards", 4, DataType.Numeric, 6, false);
            Add("356d", "NetWeight_TroyOunces", 4, DataType.Numeric, 6, false);
            Add("357d", "NetVolume_Ounces", 4, DataType.Numeric, 6, false);
            Add("360d", "NetVolume_Quarts", 4, DataType.Numeric, 6, false);
            Add("361d", "NetVolume_Gallonen", 4, DataType.Numeric, 6, false);
            Add("362d", "GrossVolume_Quarts", 4, DataType.Numeric, 6, false);
            Add("363d", "GrossVolume_Gallonen", 4, DataType.Numeric, 6, false);
            Add("364d", "NetVolume_CubicInches", 4, DataType.Numeric, 6, false);
            Add("365d", "NetVolume_CubicFeet", 4, DataType.Numeric, 6, false);
            Add("366d", "NetVolume_CubicYards", 4, DataType.Numeric, 6, false);
            Add("367d", "GrossVolume_CubicInches", 4, DataType.Numeric, 6, false);
            Add("368d", "GrossVolume_CubicFeet", 4, DataType.Numeric, 6, false);
            Add("369d", "GrossVolume_CubicYards", 4, DataType.Numeric, 6, false);
            Add("37", "QuantityInParts", 2, DataType.Numeric, 8, true);
            Add("390d", "AmountDue_DefinedValutaBand", 4, DataType.Numeric, 15, true);
            Add("391d", "AmountDue_WithISOValutaCode", 4, DataType.Numeric, 18, true);
            Add("392d", "BePayingAmount_DefinedValutaBand", 4, DataType.Numeric, 15, true);
            Add("393d", "BePayingAmount_WithISOValutaCode", 4, DataType.Numeric, 18, true);
            Add("400", "JobNumberOfGoodsRecipient", 3, DataType.Alphanumeric, 30, true);
            Add("401", "ShippingNumber", 3, DataType.Alphanumeric, 30, true);
            Add("402", "DeliveryNumber", 3, DataType.Numeric, 17, false);
            Add("403", "RoutingCode", 3, DataType.Alphanumeric, 30, true);
            Add("410", "EAN_UCC_GlobalLocationNumber(GLN)_GoodsRecipient", 3, DataType.Numeric, 13, false);
            Add("411", "EAN_UCC_GlobalLocationNumber(GLN)_InvoiceRecipient", 3, DataType.Numeric, 13, false);
            Add("412", "EAN_UCC_GlobalLocationNumber(GLN)_Distributor", 3, DataType.Numeric, 13, false);
            Add("413", "EAN_UCC_GlobalLocationNumber(GLN)_FinalRecipient", 3, DataType.Numeric, 13, false);
            Add("414", "EAN_UCC_GlobalLocationNumber(GLN)_PhysicalLocation", 3, DataType.Numeric, 13, false);
            Add("415", "EAN_UCC_GlobalLocationNumber(GLN)_ToBilligParticipant", 3, DataType.Numeric, 13, false);
            Add("420", "ZipCodeOfRecipient_withoutCountryCode", 3, DataType.Alphanumeric, 20, true);
            Add("421", "ZipCodeOfRecipient_withCountryCode", 3, DataType.Alphanumeric, 12, true);
            Add("422", "BasisCountryOfTheWares_ISO3166Format", 3, DataType.Numeric, 3, false);
            Add("7001", "Nato Stock Number", 4, DataType.Numeric, 13, false);
            Add("710", "NationalHealthcareReimbursementNumberDE", 3, DataType.Alphanumeric, 20, true);
            Add("711", "NationalHealthcareReimbursementNumberFR", 3, DataType.Alphanumeric, 20, true);
            Add("712", "NationalHealthcareReimbursementNumberES", 3, DataType.Alphanumeric, 20, true);
            Add("714", "NationalHealthcareReimbursementNumberPT", 3, DataType.Alphanumeric, 20, true);
            Add("8001", "RolesProducts", 4, DataType.Numeric, 14, false);
            Add("8002", "SerialNumberForMobilePhones", 4, DataType.Alphanumeric, 20, true);
            Add("8003", "GlobalReturnableAssetIdentifier", 4, DataType.Alphanumeric, 34, true);
            Add("8004", "GlobalIndividualAssetIdentifier", 4, DataType.Numeric, 30, true);
            Add("8005", "SalesPricePerUnit", 4, DataType.Numeric, 6, false);
            Add("8006", "IdentifikationOfAProductComponent", 4, DataType.Numeric, 18, false);
            Add("8007", "IBAN", 4, DataType.Alphanumeric, 30, true);
            Add("8008", "DataAndTimeOfManufacturing", 4, DataType.Numeric, 12, true);
            Add("8018", "GlobalServiceRelationNumber", 4, DataType.Numeric, 18, false);
            Add("8020", "NumberBillCoverNumber", 4, DataType.Alphanumeric, 25, false);
            Add("8100", "CouponExtendedCode_NSC_offerCcode", 4, DataType.Numeric, 10, false);
            Add("8101", "CouponExtendedCode_NSC_offerCcode_EndOfOfferCode", 4, DataType.Numeric, 14, false);
            Add("8102", "CouponExtendedCode_NSC", 4, DataType.Numeric, 6, false);
            Add("90", "InformationForBilateralCoordinatedApplications", 2, DataType.Alphanumeric, 30, true);
            Add("9N", "ASC PPN", 2, DataType.Alphanumeric, 22, true);
            Add("8P", "ASC GTIN", 2, DataType.Numeric, 14, true);
            Add("1T", "ASC Batch", 2, DataType.Alphanumeric, 20, true);
            Add("D", "ASC Expiry", 1, DataType.Numeric, 6, true);
            Add("S", "ASC Serial", 1, DataType.Alphanumeric, 20, true);
            aiis = aiiDict.Keys.ToArray();
            minLengthOfAI = aiiDict.Values.Min(el => el.LengthOfAI);
            maxLengthOfAI = aiiDict.Values.Max(el => el.LengthOfAI);

        }

        public void Add(string AI, string Description, int LengthOfAI, DataType DataDescription, int LengthOfData, bool FNC1)
        {
            aiiDict[AI] = new AII(AI, Description, LengthOfAI, DataDescription, LengthOfData, FNC1);
        }

        public Dictionary<string, string> Parse(string data, bool throwException = false)
        {
            if (data.StartsWith(EAN128StartCode))
                data = data.Substring(EAN128StartCode.Length);
            
            if (data.StartsWith(ASCStartCode))
                data = data.Substring(ASCStartCode.Length);

            if (HasCheckSum)
                data = data.Substring(0, data.Length - 2);

            var result = new Dictionary<string, string>();

            // data = data.ToUpper()

            int index = 0;
            while (index < data.Length)
            {
                var ai = GetAI(data, ref index);
                if (ai is null)
                {
                    if (throwException)
                        throw new InvalidOperationException("AI not found");
                    return result;
                }

                string code = GetCode(data, ai, ref index);
                result[ai.AI] = code;
            }

            return result;
        }

        private AII GetAI(string data, ref int index, bool usePlaceHolder = false)
        {
            AII result = null;
            for (int i = minLengthOfAI, loopTo = maxLengthOfAI; i <= loopTo; i++)
            {
                string ai = data.Substring(index, i);
                if (usePlaceHolder)
                    ai = ai.Remove(ai.Length - 1) + "d";
                if (aiiDict.TryGetValue(ai, out result))
                {
                    index += i;
                    return result;
                }
            }

            if (!usePlaceHolder)
                result = GetAI(data, ref index, true);
            return result;
        }

        private string GetCode(string data, AII ai, ref int index)
        {
            int dataLength;
            if (ai.FNC1)
            {
                if (data.IndexOf(GroupSeperator, index) > 0)
                {
                    dataLength = data.IndexOf(GroupSeperator, index) - index + 1;
                }
                else
                {
                    dataLength = data.Length - index;
                }
            }
            else
            {
                dataLength = ai.LengthOfData;
                if (dataLength > data.Length - index)
                {
                    dataLength = data.Length - index;
                }
            }

            string result = data.Substring(index, dataLength).Replace(Convert.ToString(GroupSeperator), string.Empty);
            index += dataLength;


            // Dim lengthToRead As Integer = Math.Min(ai.LengthOfData, data.Length - index)


            // Dim result As String = data.Substring(index, lengthToRead)

            // If ai.FNC1 Then

            // Dim indexOfGroupTermination As Integer = result.IndexOf(GroupSeperator)

            // If indexOfGroupTermination >= 0 Then
            // lengthToRead = indexOfGroupTermination + 1
            // Else
            // lengthToRead = lengthToRead + 1
            // End If


            // result = data.Substring(index, lengthToRead).Replace(GroupSeperator, String.Empty)

            // End If

            // index += lengthToRead

            return result;
        }

        private SortedDictionary<string, AII> aiiDict = new SortedDictionary<string, AII>();
        private string[] aiis;
        private int minLengthOfAI;
        private int maxLengthOfAI;
        private bool _hasCheckSum;

       public bool HasCheckSum
        {
            get
            {
                return _hasCheckSum;
            }

            set
            {
                _hasCheckSum = value;
            }
        }

        private char _groupSeperator;

        public char GroupSeperator
        {
            get
            {
                return _groupSeperator;
            }

            set
            {
                _groupSeperator = value;
            }
        }

        private string _ean128StartCode;

        public string EAN128StartCode
        {
            get
            {
                return _ean128StartCode;
            }

            set
            {
                _ean128StartCode = value;
            }
        }

        private string _ascStartCode;
        public string ASCStartCode
        {
            get
            {
                return _ascStartCode;
            }

            set
            {
                _ascStartCode = value;
            }
        }


        public DateTime GetDateFromString(string expiryDate)
        {
            int yy = int.Parse(expiryDate.Substring(0, 2)) + 2000;
            int mm = int.Parse(expiryDate.Substring(2, 2));
            int dd = int.Parse(expiryDate.Substring(4, 2));
            DateTime dt;
            if (dd > 0)
            {
                dt = new DateTime(yy, mm, dd);
            }
            else
            {
                if (mm == 12)
                {
                    yy = yy + 1;
                    mm = 1;
                }
                else
                {
                    mm = mm + 1;
                }

                dt = new DateTime(yy, mm, 1).AddDays(-1);
            }

            return dt;
        }

        private bool _isValid;

        public bool IsValid
        {
            get
            {
                return _isValid;
            }
        }


        public NMVSHelper.UIDInfo GetUIDInfo(string barcode)
        {
            _isValid = true;

            if (string.IsNullOrEmpty(barcode))
            {
                _isValid = false;
                return null;
            }

            barcode = barcode.Replace("^", "&");

            Dictionary<string, string> results;
            string uid = barcode.Trim();
            try
            {
                results = Parse(uid, true);
            }
            catch (Exception ex)
            {
                _isValid = false;
                return null;
            }

            var retval = new NMVSHelper.UIDInfo();

            if (results.ContainsKey("01"))
                retval.GTIN = results["01"];

            if (results.ContainsKey("9N"))
                retval.PPN = results["9N"];

            if (results.ContainsKey("8P"))
                retval.GTIN = results["8P"];

            if (results.ContainsKey("10"))
                retval.BatchNumber = results["10"];

            if (results.ContainsKey("1T"))
                retval.BatchNumber = results["1T"];

            if (results.ContainsKey("17"))
                retval.Expiry = results["17"];

            if (results.ContainsKey("D"))
                retval.Expiry = results["D"];
            if (retval.Expiry !=null)
            {
                retval.ExpiryDate = GetDateFromString(retval.Expiry);
            }
           

            if (results.ContainsKey("21"))
                retval.SerialNumber = results["21"];

            if (results.ContainsKey("S"))
                retval.SerialNumber = results["S"];

            if (results.ContainsKey("712"))
                retval.NHRN = results["712"];

            retval.Uid = uid;

            return retval;
        }


        public string GetUIDClientInfo(string barcode)
        {
            NMVSHelper.UIDInfo info = null;

            if (!string.IsNullOrEmpty(barcode))
                barcode = barcode.Replace("^", "&");

            info = GetUIDInfo(barcode);

            if (info is object)
            {
                var js = new JavaScriptSerializer();
                return js.Serialize(info);
            }
            else
            {
                return null;
            }
        }

    }




}
