﻿using System;
using Evolution.NMVS.MasterDataServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;

namespace Evolution.NMVS.MasterData
{
    class MasterDataPing
    {
        /// <summary>
        /// Ping request to test the connection to the MasterData-Services.
        /// Displays the XML request and the response.
        /// </summary>
        public static void PingMasterData()
        {
            //add using directive Evolution.NMVS.MasterDataServices;
            //binding the service address you want to contact
            string EndPoint = "";

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            MasterDataServicesClient service = new MasterDataServicesClient(binding, new EndpointAddress(EndPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2("", "", X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates PingRequest and set the ping Input
            MasterDataPingRequest ping = new MasterDataPingRequest();
            ping.Input = "Hello World!";

            //Creates PingResponse, result of the request.
            MasterDataPingResponse pingResponse = service.PingMasterData(ping);

            //Displays the response. If the request is successful, the output is equal to the input
            Console.WriteLine(pingResponse.Output);
        }
    }
}
