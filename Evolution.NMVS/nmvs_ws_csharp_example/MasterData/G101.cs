﻿using System;
using Evolution.NMVS.MasterDataServices;
using System.ServiceModel;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;

namespace Evolution.NMVS.MasterData
{
    class G101
    {
        public static object Header { get; set; }
        /// <summary>
        /// Creates a Master Data request
        /// </summary>
        public static void G101DownloadProductMasterData()
        {
            //add using directive Evolution.NMVS.MasterDataServices;
            //binding the service address you want to contact
            string EndPoint = "";

            //Creates a G101Request object and a G101DownloadProductMasterDataRequest object
            G101DownloadProductMasterDataRequest request = new G101DownloadProductMasterDataRequest();
            G101Request g101Request = new G101Request();

            //Executes the SetHeaderRequest method from the header class to fill the header
            object Header = Evolution.NMVS.Header.SetHeaderRequest("Evolution.NMVS.MasterDataServices", false);
            //Executes the SetMasterDataRequestBody method to fill the body
            object Body = SetMasterDataRequestBody();

            //Appends the header and body to the request object
            g101Request.Header = (RequestHeaderData_Type)Header;
            g101Request.Body = (I9_DownloadMasterdataTransactionData_Type)Body;
            request.G101Request = g101Request;

            //add using directive System.ServiceModel;
            //Defines a secure binding with certificate authentication
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;

            //Creates ServiceClient, attach transport-binding, Endpoint and the loaded certificate
            MasterDataServicesClient service = new MasterDataServicesClient(binding, new EndpointAddress(EndPoint));

            //add using directive System.Security.Cryptography.X509Certificates;
            X509Certificate2 cert = new X509Certificate2("", "", X509KeyStorageFlags.PersistKeySet);
            service.ClientCredentials.ClientCertificate.Certificate = cert;
            service.Endpoint.EndpointBehaviors.Add(new CustomMessageInspector());

            //Creates G101Response, result of the request.
            G101Response response = service.G101DownloadProductMasterData(g101Request);

            //Displays the response header and body.
            Console.WriteLine(response.Header);
            Console.WriteLine(response.Body);
        }

        /// <summary>
        /// Method to generate the body for a Master Data request
        /// </summary>
        /// <returns></returns>
        private static object SetMasterDataRequestBody()
        {
            List<ProductIdentifier_Type> list = new List<ProductIdentifier_Type>();
            ProductIdentifier_Type[] productList;

            //Set the product code and the schema of a single pack
            ProductIdentifier_Type productCode = new ProductIdentifier_Type();            
            productCode.Value = "ABCDEFG";
            productCode.scheme = CatalogProductSchema_Type.GTIN; // Alternativ PPN

            list.Add(productCode);
            productList = list.ToArray();

            //Append the productlist object to the I9_DownloadMasterdataTransactionData_Type object
            I9_DownloadMasterdataTransactionData_Type body = new I9_DownloadMasterdataTransactionData_Type();
            body.ProductCodeList = productList;
            return body;
        }
    }
}
