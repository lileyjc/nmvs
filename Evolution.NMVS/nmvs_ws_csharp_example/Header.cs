﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evolution.NMVS
{
    class Header
    {
        /// <summary>
        /// Generates, from the user data specified in the settings, the header for the individual transactions
        /// </summary>
        /// <param name="serviceName">Specify the service name to be used for the header. For example "SinglePackServices"</param>
        /// <returns></returns>
        internal static object SetHeaderRequest(string serviceName, bool TrxId,string clientId,string userId,string userPwd)
        {
            Type typeHeaderData = Type.GetType(serviceName + ".RequestHeaderData_Type");
            Type typeAuthHeaderData = Type.GetType(serviceName + ".RequestAuthHeaderData_Type");
            Type typeUserSoftware = Type.GetType(serviceName + ".UserSoftware_Type");

            Type typeTransactionHeaderData = Type.GetType(serviceName + ".RequestTransactionHeaderData_Type");

            dynamic headerData = Activator.CreateInstance(typeHeaderData);
            dynamic authHeaderData = Activator.CreateInstance(typeAuthHeaderData);
            dynamic userSoftware = Activator.CreateInstance(typeUserSoftware);
            dynamic transactionHeaderData = Activator.CreateInstance(typeTransactionHeaderData);

            //authHeaderData.ClientLoginId = "";
            //authHeaderData.UserId = "";
            //authHeaderData.Password = "";

            authHeaderData.ClientLoginId = clientId;
            authHeaderData.UserId = userId;
            authHeaderData.Password = userPwd;


            if (TrxId == true)
            {
                authHeaderData.SubUserId = "06c8dea069c24446949e7806e028bcaf"; //Alternativ: "";
            }

            headerData.Auth = authHeaderData;
            headerData.Transaction = transactionHeaderData;
            headerData.UserSoftware = userSoftware;

            //userSoftware.name = "";
            //userSoftware.supplier = "";
            //userSoftware.version = "";

            userSoftware.name = "vSoft";
            userSoftware.supplier = "Visual Software";
            userSoftware.version = "1.20";


            //transactionHeaderData.ClientTrxId = "06c8dea069c24446949e7806e028bcaf";  //Alternativ Guid.NewGuid().ToString().Replace("-", "");
            transactionHeaderData.ClientTrxId = Guid.NewGuid().ToString().Replace("-", String.Empty);

            //transactionHeaderData.Language = "";
            transactionHeaderData.Language = "eng";

            return headerData;
        }

        internal static object SetHeaderRequest(string serviceName, bool TrxId)
        {

            Type typeHeaderData = Type.GetType(serviceName + ".RequestHeaderData_Type");
            Type typeAuthHeaderData = Type.GetType(serviceName + ".RequestAuthHeaderData_Type");
            Type typeUserSoftware = Type.GetType(serviceName + ".UserSoftware_Type");

            Type typeTransactionHeaderData = Type.GetType(serviceName + ".RequestTransactionHeaderData_Type");

            dynamic headerData = Activator.CreateInstance(typeHeaderData);
            dynamic authHeaderData = Activator.CreateInstance(typeAuthHeaderData);
            dynamic userSoftware = Activator.CreateInstance(typeUserSoftware);
            dynamic transactionHeaderData = Activator.CreateInstance(typeTransactionHeaderData);

            authHeaderData.ClientLoginId = "";
            authHeaderData.UserId = "";
            authHeaderData.Password = "";

            if (TrxId == true)
            {
                authHeaderData.SubUserId = "06c8dea069c24446949e7806e028bcaf"; //Alternativ: "";
            }

            headerData.Auth = authHeaderData;
            headerData.Transaction = transactionHeaderData;
            headerData.UserSoftware = userSoftware;

            userSoftware.name = "";
            userSoftware.supplier = "";
            userSoftware.version = "";

            transactionHeaderData.ClientTrxId = "06c8dea069c24446949e7806e028bcaf";  //Alternativ Guid.NewGuid().ToString().Replace("-", "");
            transactionHeaderData.Language = "";

            return headerData;

        }


        }
}
